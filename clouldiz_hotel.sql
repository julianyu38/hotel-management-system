-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 15, 2018 at 10:39 AM
-- Server version: 10.1.35-MariaDB-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `clouldiz_hotel`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_user`
--

CREATE TABLE `admin_user` (
  `id` int(111) NOT NULL,
  `username` varchar(111) NOT NULL,
  `password` varchar(111) NOT NULL,
  `email` varchar(225) NOT NULL,
  `web_email` varchar(255) NOT NULL,
  `contact` varchar(111) NOT NULL,
  `admin_logo` varchar(500) NOT NULL,
  `contact_address` varchar(255) NOT NULL,
  `contact_email` varchar(255) NOT NULL,
  `contact_phone` varchar(255) NOT NULL,
  `info` varchar(50000) NOT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_user`
--

INSERT INTO `admin_user` (`id`, `username`, `password`, `email`, `web_email`, `contact`, `admin_logo`, `contact_address`, `contact_email`, `contact_phone`, `info`, `modified_on`) VALUES
(1, 'admin', '123456', 'admin@hotel.com', 'info@mobidudes.com', '+10-456789654', '', '405,onam plaza ,near industry house, palasiya', 'test@ttest.com', 'xxx-xxx-7984', '<p>Global Currency</p>\r\n', '2018-10-02 06:58:12');

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `id` int(111) NOT NULL,
  `user_id` int(111) NOT NULL,
  `hotel_id` int(111) NOT NULL,
  `room_type` int(11) NOT NULL,
  `price_per_night` double(10,2) NOT NULL,
  `no_of_booking_room` int(11) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`id`, `user_id`, `hotel_id`, `room_type`, `price_per_night`, `no_of_booking_room`, `from_date`, `to_date`, `created_on`) VALUES
(1, 2, 1, 1, 20.00, 1, '2018-10-01', '2018-10-03', '2018-10-01 12:44:22'),
(2, 2, 5, 1, 40.00, 1, '2018-10-01', '2018-10-04', '2018-10-01 12:48:38'),
(3, 7, 3, 1, 12.00, 1, '2018-10-02', '2018-10-12', '2018-10-02 11:18:47'),
(9, 8, 3, 3, 12.00, 1, '2018-10-12', '2018-10-02', '2018-10-03 09:17:15'),
(12, 8, 2, 2, 19.00, 1, '2018-10-13', '2018-10-25', '2018-10-03 09:34:26'),
(13, 8, 3, 3, 12.00, 1, '2018-10-12', '2018-10-19', '2018-10-03 09:34:48'),
(15, 10, 4, 1, 25.00, 1, '2018-10-03', '2018-10-06', '2018-10-03 07:08:47'),
(16, 10, 2, 1, 19.00, 1, '2018-10-03', '2018-10-06', '2018-10-03 07:11:06');

-- --------------------------------------------------------

--
-- Table structure for table `debth`
--

CREATE TABLE `debth` (
  `id` int(11) NOT NULL,
  `hotel1` double(10,2) NOT NULL,
  `hotel2` double(10,2) NOT NULL,
  `hotel3` double(10,2) NOT NULL,
  `hotel4` double(10,2) NOT NULL,
  `hotel5` double(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `debth`
--

INSERT INTO `debth` (`id`, `hotel1`, `hotel2`, `hotel3`, `hotel4`, `hotel5`) VALUES
(1, 1.20, 1.90, 3.40, 2.50, 4.00),
(2, 0.80, 4.00, 3.70, 1.80, 3.90),
(3, 2.70, 2.60, 1.00, 4.20, 3.50),
(4, 2.10, 3.70, 1.30, 4.10, 2.90),
(5, 3.20, 4.20, 2.00, 2.50, 3.80);

-- --------------------------------------------------------

--
-- Table structure for table `hotels`
--

CREATE TABLE `hotels` (
  `id` int(111) NOT NULL,
  `name` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `manager` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hotels`
--

INSERT INTO `hotels` (`id`, `name`, `category`, `address`, `phone`, `manager`) VALUES
(1, 'Hotel 1', '1* Hotel', 'Tourlos Mykonos, Mykonos, Greece', '+30 2289985634', 'Mrs. Kalypso Makrygianni'),
(2, 'Hotel 2', '2* Hotel', 'Bouboulinas 1, Athens, Greece', '+30 210581296', ' Mrs. Maria Grammatikopoulou'),
(3, 'Hotel 3', '1* Hotel', 'Pyrgos Thermi / Mytilini Area, Pyrgi Thermis, Lesvos, Greece', '+30 2251 157968', 'Mr. Thanasis Papathanasiou'),
(4, 'Hotel 4', '4* Hotel', 'Glyfada Beach, Logos, Paxoi, Greece', '+30 2662123456', 'Mr. Grigoris Natsios'),
(5, 'Hotel 5', '5* Hotel', 'Tourlos Mykonos, Mykonos, Greece', '+30 2289985634', ' Mrs. Kalypso Makrygianni');

-- --------------------------------------------------------

--
-- Table structure for table `no_of_room`
--

CREATE TABLE `no_of_room` (
  `id` int(111) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `room_type` int(111) NOT NULL,
  `number` int(11) NOT NULL,
  `price` double(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `no_of_room`
--

INSERT INTO `no_of_room` (`id`, `hotel_id`, `room_type`, `number`, `price`) VALUES
(1, 1, 1, 1, 20.00),
(2, 1, 2, 1, 30.00),
(3, 1, 3, 1, 40.00),
(4, 1, 4, 1, 100.00),
(5, 2, 1, 1, 19.00),
(6, 2, 2, 1, 28.00),
(7, 2, 3, 1, 37.00),
(8, 2, 4, 1, 95.00),
(9, 3, 1, 1, 12.00),
(10, 3, 2, 1, 20.00),
(11, 3, 3, 1, 30.00),
(12, 3, 4, 1, 80.00),
(13, 4, 1, 1, 25.00),
(14, 4, 2, 1, 40.00),
(15, 4, 3, 1, 55.00),
(16, 4, 4, 1, 180.00),
(17, 5, 1, 1, 40.00),
(18, 5, 2, 1, 60.00),
(19, 5, 3, 1, 80.00),
(20, 5, 4, 1, 220.00);

-- --------------------------------------------------------

--
-- Table structure for table `room_type`
--

CREATE TABLE `room_type` (
  `id` int(111) NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `room_type`
--

INSERT INTO `room_type` (`id`, `type`) VALUES
(1, 'Single Room'),
(2, 'Double Room'),
(3, 'Triple Room'),
(4, 'Suite ');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(111) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `credit_number` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `unique_id`, `username`, `name`, `email`, `password`, `contact`, `address`, `credit_number`, `status`, `created_on`, `updated_on`) VALUES
(1, '', '', 'Maria Papadopoulou', 'mariapap@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', '6989858782', 'Kalidopoulou 12, Thessaloniki', '123123123', 0, '2018-09-28 02:18:11', '0000-00-00 00:00:00'),
(2, '5baf5795c206d', 'kostbell', 'Kostas Bellos', 'kotsbell@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', '6983698745', 'Panapastasiou 27, Thessaloniki', '12312231234121212', 0, '2018-10-02 06:01:43', '2018-10-02 06:01:43'),
(3, '5bb0ae5cf1eb1', 'petrakis', 'Petros', 'petrakis@hotmail.com', '454f21c3fab38209f50e711af041b6044b8d18b8', '6987459328', 'peukon 32, thessaloniki', '1285478596', 0, '2018-09-30 11:07:08', '0000-00-00 00:00:00'),
(4, '5bb247a8b8825', 'rania1', 'rania', 'rania@hotmail.com', '0705b0e32124f4617627e63158e0f4a21018066d', '6984736829', 'odisou 3', '4567890987', 0, '2018-10-01 16:13:28', '0000-00-00 00:00:00'),
(5, '5bb30edcde97a', 'mimis11', 'Mimis Georgiou', 'mimisgeorgiou@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', '6936359467', 'Pasalidou 39, Thessaloniki', '123456789012345', 0, '2018-10-02 06:23:53', '2018-10-02 06:23:53'),
(6, '5bb352d91689f', 'nikolakias', 'Nikos Petrou', 'nikolaos@gmail.com', '874d68bdf3312d596f6a621ee0c7586b1bd23a8f', '6987469523', 'Petrou sundika 1', '5478963258', 0, '2018-10-02 11:13:29', '0000-00-00 00:00:00'),
(7, '5bb353747ebcc', 'gatino', 'Georgia Tinou', 'gatito@hotmail.com', 'ff067dbacf279d57aa1858bb9a949394fab0421c', '6985749685', 'iliados 3', '1234567890', 0, '2018-10-02 11:16:04', '0000-00-00 00:00:00'),
(8, '5bb45ffe910e2', 'mariatsi', 'Marios Gerolimnios', 'mariatsi@gmail.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', '6917182839', 'ygujkgyujgbh', '8787878', 0, '2018-10-03 06:21:50', '0000-00-00 00:00:00'),
(9, '5bb50fb19c1e2', 'mariap', 'Maria Petrou', 'mariapetrou@gmail.com', '8cb2237d0679ca88db6464eac60da96345513964', '6987452147', 'Odisou 3', '1254789632', 0, '2018-10-03 18:52:36', '2018-10-03 06:52:36'),
(10, '5bb51314aefc1', 'tasos@1', 'Tasos Karatasos', 'tasoskarata@gmail.com', '8cb2237d0679ca88db6464eac60da96345513964', '6987458797', 'Adrianoupoleos 19', '1234567890', 0, '2018-10-03 19:07:01', '2018-10-03 07:07:01'),
(11, '5bc48540cd68a', 'dk', 'dk', 'admin@gmail.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', '898989898', 'dhg', '89898989898', 0, '2018-10-15 12:17:04', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_user`
--
ALTER TABLE `admin_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `debth`
--
ALTER TABLE `debth`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hotels`
--
ALTER TABLE `hotels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `no_of_room`
--
ALTER TABLE `no_of_room`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `room_type`
--
ALTER TABLE `room_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `hotels`
--
ALTER TABLE `hotels`
  MODIFY `id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `no_of_room`
--
ALTER TABLE `no_of_room`
  MODIFY `id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `room_type`
--
ALTER TABLE `room_type`
  MODIFY `id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
