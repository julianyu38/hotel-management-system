  <!--FOOTER-->
        <footer class="site-footer footer-default">
            <div class="copyright-area">
                <div class="container">
                    <div class="copyright-content">
                        <!--<p class="copyright-text">-->
                        <!--    <span>Hotel</span> .Designed by <span><a href="#">ONOMATEPONUMO</a></span>-->
                        <!--</p>-->
                    </div>
                </div>
            </div>
        </footer>
        <!--END FOOTER-->

    </div>

</div>
<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo base_url('frontend/')?>js/jquery.min.js"></script>
<script src="<?php echo base_url('frontend/')?>js/jquery1.min.js"></script>
<script src="<?php echo base_url('frontend/')?>plugin/dist/owl.carousel.min.js"></script>
<script src="<?php echo base_url('frontend/')?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url('frontend/')?>js/jquery.waypoints.js"></script>
<script src="<?php echo base_url('frontend/')?>js/number-count/jquery.counterup.min.js"></script>
<script src="<?php echo base_url('frontend/')?>js/isotope.pkgd.min.js"></script>
<script src="<?php echo base_url('frontend/')?>js/jquery-ui.min.js"></script>
<script src="<?php echo base_url('frontend/')?>js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url('frontend/')?>js/bootstrap-datepicker.tr.min.js"></script>
<script src="<?php echo base_url('frontend/')?>js/moment.min.js"></script>
<script src="<?php echo base_url('frontend/')?>js/wow.min.js"></script>
<script src="<?php echo base_url('frontend/')?>js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script src="<?php echo base_url('frontend/')?>js/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>
<script src="<?php echo base_url('frontend/')?>js/picturefill.min.js"></script>
<script src="<?php echo base_url('frontend/')?>js/lightgallery.js"></script>
<script src="<?php echo base_url('frontend/')?>js/lg-pager.js"></script>
<script src="<?php echo base_url('frontend/')?>js/lg-autoplay.js"></script>
<script src="<?php echo base_url('frontend/')?>js/lg-fullscreen.js"></script>
<script src="<?php echo base_url('frontend/')?>js/lg-zoom.js"></script>
<script src="<?php echo base_url('frontend/')?>js/lg-hash.js"></script>
<script src="<?php echo base_url('frontend/')?>js/lg-share.js"></script>
<script src="<?php echo base_url('frontend/')?>js/jquery.nice-select.js"></script>
<script src="<?php echo base_url('frontend/')?>js/semantic.js"></script>
<script src="<?php echo base_url('frontend/')?>js/parallax.min.js"></script>
<script src="<?php echo base_url('frontend/')?>js/jquery.nicescroll.min.js"></script>
<script src="<?php echo base_url('frontend/')?>js/jquery.sticky.js"></script>
<script src="<?php echo base_url('frontend/')?>js/main.js"></script>

</body>

</html>