<?php include('header.php');?>

        <!--BENGIN CONTENT HEADER-->
        <section class="site-content-area mrg_top">
            <div class="container-fluid">
                <div class="row">
                    <div class="vk-contact-form">
                        <div class="container">
                            <div class="vk-contact-form-info-header">
                                <h2>Login</h2>
                                <div class="clearfix"></div>
                                <div class="vk-contact-border"></div>
                            </div>
                            <div class="vk-contact-form-info-body">
							<?php if($this->session->flashdata("errmessage")){ ?>
								<p class="text-danger-Invalid" style="text-align:center;color:red;"><?php echo $this->session->flashdata("errmessage")?></p>
							<?php } ?> 
                               
                                <div class="login_inside">
                                    <form action="" method="post">
                                        <div class="form-group">
                                            <label> Username </label>
                                            <input type="text" placeholder="Username" id="email" name="email" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label> Password </label>
                                            <input type="password" placeholder="Password" id="password" name="password" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" name="login" value="login" class="vk-btn  vk-btn-xs vk-btn-default text-uppercase" onClick="Logincheck();">
                                                Login
                                            </button>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--END CONTENT ABOUT-->
<?php include('footer.php');?>
 