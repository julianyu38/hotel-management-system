
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Hotel </title>
    <link rel="stylesheet" href="<?php echo base_url('frontend/')?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url('frontend/')?>css/bootstrap-theme.min.css">
    <link href="<?php echo base_url('frontend/')?>fonts/raleway/raleway.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url('frontend/')?>fonts/font-awesome/css/font-awesome.css">
    <link href="<?php echo base_url('frontend/')?>fonts/playfair-display/playfair-display.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url('frontend/')?>plugin/dist/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url('frontend/')?>plugin/dist/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php echo base_url('frontend/')?>css/themify-icons.css">
    <link rel="stylesheet" href="<?php echo base_url('frontend/')?>css/animate.css">
    <link rel="stylesheet" href="<?php echo base_url('frontend/')?>css/bootstrap-datepicker3.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('frontend/')?>css/jquery-ui.min.css">
    <link rel="stylesheet" href="<?php echo base_url('frontend/')?>css/nice-select.css">
    <link href="<?php echo base_url('frontend/')?>css/lightgallery.css" rel="stylesheet">
    <link href="<?php echo base_url('frontend/')?>css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('frontend/')?>css/semantic.css">
    <link rel="stylesheet" href="<?php echo base_url('frontend/')?>css/paraxify.css">
    <link rel="stylesheet" href="<?php echo base_url('frontend/')?>css/style.css">


</head>
<body class="">

<!--load page-->
<div class="load-page">
    <div class="spinner">
        <div class="rect1"></div>
        <div class="rect2"></div>
        <div class="rect3"></div>
        <div class="rect4"></div>
        <div class="rect5"></div>
    </div>
</div>

<!-- Mobile nav -->
<nav class="visible-sm visible-xs mobile-menu-container mobile-nav vk-menu-mobile-nav">
    <div class="menu-mobile-nav navbar-toggle">
        <span class="icon-bar"><i class="fa fa-bars" aria-hidden="true"></i></span>
    </div>
    <div id="cssmenu" class="animated" style="display: none;"><div id="menu-line" style="width: 0px; left: 0px;"></div>
        <div class="uni-icon-close">
            <span class="ti-close"></span>
        </div>
        <ul class="nav navbar-nav">
            <?php
            if(isset($_SESSION['admin']['logged_in']) && $_SESSION['admin']['logged_in']!=''){
            ?>
            <li><a href="<?php echo base_url('Home/logout')?>">Logout</a></li>
            <?php } ?>
             <li><a href="<?php echo base_url()?>">Home</a></li>
              <li><a href="<?php echo base_url('Home/hotel')?>">Hotel</a></li>
               
                                    
									<?php
									if(isset($_SESSION['user']['user_id']) && $_SESSION['user']	['user_id']!=''){
									?>
									<li><a href="<?php echo base_url('Booking')?>">Book</a></li>
									<li><a href="<?php echo base_url('Home/profile')?>">profile</a></li>
										<li><a href="<?php echo base_url('Home/reservation')?>">Upcoming Reservation</a></li>
									<li><a href="<?php echo base_url('Home/logout')?>">Logout</a></li>
                                   
									<?php }else{ ?>
									<li><a href="<?php echo base_url('Home/login')?>">Login</a></li>
                                    <li><a href="<?php echo base_url('Home/signup')?>">Signup</a></li>
									<?php } ?>

           
        </ul>
        <div class="clearfix"></div>
    </div>
</nav>
<!-- End mobile menu -->





<div id="wrapper-container" class="site-wrapper-container">
    <header class="site-header header-default header-sticky ">
         <div id="sticky-wrapper" class="sticky-wrapper" style="height: 100px;"><div class="vk-main-menu animated uni-sticky" style="width: 1349px;">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 col-xs-9">
                        <div class="wrapper-logo">
                            <a href="<?php echo base_url()?>" class="logo-default"><img src="<?php echo base_url('frontend/')?>img/logo.png" alt="" class="img-responsive"></a>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <nav class="main-navigation">
                            <div class="inner-navigation">
                                <ul class="nav-bar pull-right">
                                    
                                    <li><a href="<?php echo base_url()?>">Home</a></li>
                                    <li><a href="<?php echo base_url('Home/hotel')?>">Hotel</a></li>
                                   
									<?php
									if(isset($_SESSION['user']['user_id']) && $_SESSION['user']	['user_id']!=''){
									?>
									<li><a href="<?php echo base_url('Booking')?>">Book</a></li>
									<li><a href="<?php echo base_url('Home/profile')?>">profile</a></li>
										<li><a href="<?php echo base_url('Home/reservation')?>">Upcoming Reservation</a></li>
									<li><a href="<?php echo base_url('Home/logout')?>">Logout</a></li>
                                   
									<?php }elseif(isset($_SESSION['admin']['logged_in']) && $_SESSION['admin']['logged_in']!=''){?>
								
                                    <li><a href="<?php echo base_url('Home/logout')?>">Logout</a></li>
                                    
									<?php }else{ ?>
									<li><a href="<?php echo base_url('Home/login')?>">Login</a></li>
                                    <li><a href="<?php echo base_url('Home/signup')?>">Signup</a></li>
									<?php } ?>
                                   
                                    <!-- <li class="vk-icon-search">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </li> -->
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div></div>
    </header>
        <!--ENAD HEADER-->