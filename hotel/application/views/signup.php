<?php include('header.php'); ?>

        <!--BENGIN CONTENT HEADER-->
<style>
.text-danger-alreadyexist{
	
	color: #FF0000;
	/*background-color: #e2c672;*/
	font-size: 15px;
	padding: 4px;
	
	text-align: left;
	
}
</style>
        <section class="site-content-area mrg_top">
            <div class="container-fluid">
                <div class="row">
                    <div class="vk-contact-form">
                        <div class="container">
                            <div class="vk-contact-form-info-header">
                                <h2>Sign Up</h2>
                                <div class="clearfix"></div>
                                <div class="vk-contact-border"></div>
                            </div>
                            <div class="vk-contact-form-info-body">
							<?php if($this->session->flashdata("errmessage")){ ?>
								<p class="text-danger-alreadyexist" style="text-align:center;color:red;"><?php echo $this->session->flashdata("errmessage")?></p>
							<?php } ?> 
                                <div class="login_inside">
                                    <form action="" method="post">
                                        <div class="form-group">
                                            <label> Name </label>
                                            <input type="text" placeholder="Name" id="fullname" name="name" class="form-control" required>
                                        </div>
										<div class="form-group">
                                            <label> Username </label>
                                            <input type="text" placeholder="Username" id="username" name="username" class="form-control" required>
                                        </div>
                                        <div class="form-group">
                                            <label> Address </label>
                                            <input type="text" placeholder="Address" id="address" name="address" class="form-control" required>
                                        </div>
                                        <div class="form-group">
                                            <label> Phone Number </label>
                                            <input type="number" placeholder="Phone Number" id="contact" name="contact" class="form-control" required>
                                        </div>
                                        <div class="form-group">
                                            <label> Credit Number </label>
                                            <input type="number" placeholder="Creadit Number" id="credit_number" name="credit_number" class="form-control" required>
                                        </div>
                                        <div class="form-group">
                                            <label> Email </label>
                                            <input type="email" placeholder="Email" id="email" name="email" class="form-control" required>
											<span class="text-danger-alreadyexist" style="display:none;">Email id Already Exist</span>
                                        </div>
                                        <div class="form-group">
                                            <label> Password </label>
                                            <input type="password" placeholder="Password" id="password" name="password" class="form-control" required>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" name="save" value="save" class="vk-btn  vk-btn-xs vk-btn-default text-uppercase" onclick="checkValidation();">
                                                Submit
                                            </button>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--END CONTENT ABOUT-->

      <?php include('footer.php'); ?>
	 
	 
	   <script>
  $(document).keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
       checkValidation();   
    }
});

function checkValidation(){
   flag=0;
   
   var passwordlength=$.trim($('#password').val()).length;
   if($.trim($("input[name=name]").val())==''){
				$('#fullname').css('border', '1px solid red');
				//$('.text-danger-name').show();
				flag++;
				} else {
				$('#fullname').css('border', '');
				//$('.text-danger-name').hide();
				}
				
				
				if($.trim($("input[name=contact]").val())==''){
				$('#contact').css('border', '1px solid red');
				//$('.text-danger-name').show();
				flag++;
				} else {
				$('#contact').css('border', '');
				//$('.text-danger-name').hide();
				}
				
				if($.trim($("input[name=email]").val())==''){
				$('#email').css('border', '1px solid red');
				//$('.text-danger-name').show();
				flag++;
				} else {
				$('#email').css('border', '');
				//$('.text-danger-name').hide();
				}
				
				
				if($.trim($("input[name=password]").val())==''){
				$('#password').css('border', '1px solid red');
				//$('.text-danger-name').show();
				flag++;
				} else {
				$('#password').css('border', '');
				//$('.text-danger-name').hide();
				}
				if($.trim($("input[name=credit_number]").val())==''){
				$('#credit_number').css('border', '1px solid red');
				//$('.text-danger-name').show();
				flag++;
				} else {
				$('#credit_number').css('border', '');
				//$('.text-danger-name').hide();
				}
				if($.trim($("input[name=address]").val())==''){
				$('#address').css('border', '1px solid red');
				//$('.text-danger-name').show();
				flag++;
				} else {
				$('#address').css('border', '');
				//$('.text-danger-name').hide();
				}
			//alert(flag);	
	    if(flag==0){
			var postval = { "fullname":$("#fullname").val(), "contact":$("#contact").val(), "email":$("#email").val(),"password":$("#password").val(),"credit_number":$("#credit_number").val(),"address":$("#address").val()};
			$.ajax({
				url:"<?php echo SITE_URL; ?>saveSignup.php",
				method:"POST",
				dataType:'JSON',
				data:postval,
				success:function(responceText){
					//alert(responceText);
			    if(responceText.class == 'success'){
                    //  alert(responceText.msg);
					$('.text-success-message').show();
					 
                   setTimeout(function() {
                      $('.text-success-message').fadeOut('fast');
					   window.location.href='login.php'; 

                       }, 1000); 
					return false;
					}else{
						alert('yy');
			 //$("#"+responceText.field).css('border', '1px solid red');
			$('.text-danger-alreadyexist').show();
			$('#email').val('');
			$('#email').focus();
						//$.toaster({ priority : 'danger', title : 'Error', message : responceText.msg });
					}// Else Ends Here...
			  	}
		  });	
		}else{
			//$.toaster({ priority : 'danger', title : 'Error', message : 'Please fill mandatory field.' });
		}
}
<!-- Logincheck  Code Here.... -->
</script>