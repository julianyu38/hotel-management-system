<?php include('header.php')?>
        <!--BENGIN CONTENT HEADER-->
        <section class="site-content-area mrg_top">
            <div class="container-fluid">
                <div class="row">
                    <div class="vk-contact-form">
                        <div class="container">
                            <div class="vk-contact-form-info-header">
                                <h2>Edit Information</h2>
                                <div class="clearfix"></div>
                                <div class="vk-contact-border"></div>
                            </div>
                            <div class="vk-contact-form-info-body">
                                
                                <div class="login_inside">
                                    <form action="" method="post">
                                        <div class="form-group">
                                            <label> Name </label>
                                            <input type="text" placeholder="Name" name="name" class="form-control" value="<?php echo $view->name?>" required>
                                        </div>
                                        <div class="form-group">
                                            <label> Address </label>
                                            <input type="text" placeholder="Address" name="address" class="form-control" value="<?php echo $view->address?>" required>
                                        </div>
                                        <div class="form-group">
                                            <label> Phone Number </label>
                                            <input type="number" placeholder="Phone  Number" name="contact" class="form-control" value="<?php echo $view->contact?>" required>
                                        </div>
                                        <div class="form-group">
                                            <label> Unique Code </label>
                                            <input type="text" placeholder="Personal Id" name="unique_id" class="form-control" value="<?php echo $view->unique_id?>" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label> Credit Number </label>
                                            <input type="number" placeholder="Credit Number" name="credit_number" class="form-control" value="<?php echo $view->credit_number?>" required>
                                        </div>
                                        <div class="form-group">
                                            <label> Email </label>
                                            <input type="email" placeholder="Email" name="email" class="form-control" value="<?php echo $view->email?>" required>
                                        </div>
                                        <div class="form-group">
                                            <button  class="vk-btn  vk-btn-xs vk-btn-default text-uppercase" type="submit" name="userSubmit" value="userSubmit">
                                                <span class="title">Update</span>
                                            </button>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--END CONTENT ABOUT-->

     <?php include('footer.php')?>