<?php include('database.php');?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Hotel </title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">

    <link href="fonts/raleway/raleway.css" rel="stylesheet">
    <link rel="stylesheet" href="fonts/font-awesome/css/font-awesome.css">
    <link href="fonts/playfair-display/playfair-display.css" rel="stylesheet">

    <link rel="stylesheet" href="plugin/dist/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="plugin/dist/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/themify-icons.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/bootstrap-datepicker3.css">
    <link rel="stylesheet" type="text/css" href="css/jquery-ui.min.css">
    <link rel="stylesheet" href="css/nice-select.css">
    <link href="css/lightgallery.css" rel="stylesheet">
    <link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" type="text/css" href="css/semantic.css">
    <link rel="stylesheet" href="css/paraxify.css">
    <link rel="stylesheet" href="css/style.css">
    
<style>
    
#sticky-wrapper{position: relative;}

</style>

</head>
<body class="" onload="loadvideo()">

<!--load page-->
<div class="load-page">
    <div class="spinner">
        <div class="rect1"></div>
        <div class="rect2"></div>
        <div class="rect3"></div>
        <div class="rect4"></div>
        <div class="rect5"></div>
    </div>
</div>

<div id="wrapper-container" class="site-wrapper-container">
    <header class="site-header header-default header-sticky ">
        <div class="vk-main-menu animated uni-sticky">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 col-xs-2">
                        <div class="wrapper-logo">
                            <a href="index.php" class="logo-default"><img src="img/logo.png" alt="" class="img-responsive"></a>
                        </div>
                    </div>
                    <div class="col-md-10 col-xs-10">
                        <nav class="main-navigation">
                            <div class="inner-navigation">
                                <ul class="nav-bar pull-right">
                                    <li><a href="index.php">Home</a></li>
                                    <li><a href="hotel.php">Hotel</a></li>
									<?php
									if(isset($_SESSION['user_id']) && $_SESSION['user_id']!=''){
									?>
									<li><a href="logout.php">Logout</a></li>
                                   
									<?php }else{ ?>
									<li><a href="login.php">Login</a></li>
                                    <li><a href="signup.php">Signup</a></li>
									<?php } ?>
                                   
                                    <!-- <li class="vk-icon-search">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </li> -->
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div id="main-content" class="site-main-content">
        <div id="home-main-content" class="site-home-main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="vk-slide">
                        <div id="owl-slide-home" class="owl-carousel owl-theme animated">
                            <div class="item animated">
                                <div class="vk-item-slide">
                                    <img src="img/banner.jpg" alt="" class="img-responsive">
                                    <div class="vk-slide-caption animated">
                                        <h3 class="animated rotateInDownRight slide-delay-1">  Hotal Management System </h3>
                                        <h2 class="animated fadeInUpBig slide-delay-2"> Welcome to our Hotel Management System </h2>
                                    </div>
                                </div>
                            </div>
                            <div class="item  animated">
                                <div class="vk-item-slide">
                                    <img src="img/banner.jpg" alt="" class="img-responsive">
                                    <div class="vk-slide-caption animated">
                                        <h3 class="animated rotateInDownRight slide-delay-1">  Hotal Management System </h3>
                                        <h2 class="animated fadeInUpBig slide-delay-2"> Welcome to our Hotel Management System </h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="vk-sparta-about">
                        <div class="container">
                            <div class="vk-sparta-head-title">
                                <h3>WELCOME</h3>
                                <h2>To our Hotel Management System</h2>
                                <div class="vk-sparta-about-border"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-10 col-md-offset-1">
                                    <div class="vk-sparta-about-text">
                                        <p>
                                            This is our Hotel Management System in Codeigniter php, with CMS. All the points of the description have been covered for 5 hotels with different informations.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="vk-our-room">
                        <div class="container">
                            <div class="vk-sparta-head-title">
                                <h2>Our Hotels</h2>
                                <div class="vk-sparta-about-border"></div>
                            </div>
                            <div class="vk-spartar-our-room-destop">
                                <div class="vk-sparta-our-room">
                                    <div id="vk-owl-our-room" class="vk-owl-three-dots owl-carousel owl-theme">
                                        <div class="item">
                                            <div class="vk-sparta-item-content">
                                                <div class="vk-item-img">
                                                    <a href="#"><img src="img/img.jpg" alt="" class="img-responsive"></a>
                                                </div>
                                                <div class="vk-item-text hotel_text">
                                                    <h2><a href="#">Hotel 1</a></h2>
                                                    <p> 3* Hotel </p>
                                                    <p> Platy Beach, Limnos, Greece </p>
                                                    <a href="hotel1.php" class="vk-btn vk-btn-xs vk-btn-default text-uppercase">
                                                        <span class="title">View</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="vk-sparta-item-content">
                                                <div class="vk-item-img">
                                                    <a href="#"><img src="img/img1.jpg" alt="" class="img-responsive"></a>
                                                </div>
                                                <div class="vk-item-text hotel_text">
                                                    <h2><a href="#">Hotel 2</a></h2>
                                                    <p> 2* Hotel </p>
                                                    <p> Bouboulinas 1, Athens, Greece </p>
                                                    <a href="hotel2.php" class="vk-btn vk-btn-xs vk-btn-default text-uppercase">
                                                        <span class="title">View</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="vk-sparta-item-content">
                                                <div class="vk-item-img">
                                                    <a href="#"><img src="img/img2.jpg" alt="" class="img-responsive"></a>
                                                </div>
                                                <div class="vk-item-text hotel_text">
                                                    <h2><a href="#">Hotel 3</a></h2>
                                                    <p> 1* Hotel </p>
                                                    <p> Pyrgos Thermi / Mytilini Area, Pyrgi... </p>
                                                    <a href="hotel3.php" class="vk-btn vk-btn-xs vk-btn-default text-uppercase">
                                                         <span class="title">View</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="vk-sparta-item-content">
                                                <div class="vk-item-img">
                                                    <a href="#"><img src="img/img.jpg" alt="" class="img-responsive"></a>
                                                </div>
                                                <div class="vk-item-text hotel_text">
                                                    <h2><a href="#">Hotel 4</a></h2>
                                                    <p> 4* Hotel </p>
                                                    <p>Glyfada Beach, Logos, Paxoi, Greece</p>
                                                    <a href="hotel4.php" class="vk-btn vk-btn-xs vk-btn-default text-uppercase">
                                                         <span class="title">View</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="vk-sparta-item-content">
                                                <div class="vk-item-img">
                                                    <a href="#"><img src="img/img1.jpg" alt="" class="img-responsive"></a>
                                                </div>
                                                <div class="vk-item-text hotel_text">
                                                    <h2><a href="#">Hotel 5</a></h2>
                                                    <p> 5* Hotel </p>
                                                    <p> Tourlos Mykonos, Mykonos, Greece </p>
                                                    <a href="hotel5.php" class="vk-btn vk-btn-xs vk-btn-default text-uppercase">
                                                         <span class="title">View</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="vk-spartar-our-room-mobile">
                                <div class="item">
                                    <div class="vk-sparta-item-content">
                                        <div class="vk-item-img">
                                            <a href="#"><img src="images/01_01_default/our-room/img.jpg" alt="" class="img-responsive"></a>
                                        </div>
                                        <div class="vk-item-text">
                                            <h2><a href="#">Class Rooms</a></h2>
                                            <ul>
                                                <li>
                                                    <p>Starting Form : </p>
                                                </li>
                                                <li>
                                                    <p>$200/ <span>Night</span></p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="vk-sparta-item-content">
                                        <div class="vk-item-img">
                                            <a href="#"><img src="images/01_01_default/our-room/img1.jpg" alt="" class="img-responsive"></a>
                                        </div>
                                        <div class="vk-item-text">
                                            <h2><a href="#">Double Rooms</a></h2>
                                            <ul>
                                                <li>
                                                    <p>Starting Form : </p>
                                                </li>
                                                <li>
                                                    <p>$200/ <span>Night</span></p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="vk-sparta-item-content">
                                        <div class="vk-item-img">
                                            <a href="#"><img src="images/01_01_default/our-room/img2.jpg" alt="" class="img-responsive"></a>
                                        </div>
                                        <div class="vk-item-text">
                                            <h2><a href="#">Superior Rooms</a></h2>
                                            <ul>
                                                <li>
                                                    <p>Starting Form : </p>
                                                </li>
                                                <li>
                                                    <p>$200/ <span>Night</span></p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <footer class="site-footer footer-default">
        <div class="copyright-area">
            <div class="container">
                <div class="copyright-content">
                    <!--<p class="copyright-text">-->
                    <!--    <span>Hotel</span> .Designed by <span><a href="#">ONOMATEPONUMO</a></span>-->
                    <!--</p>-->
                </div>
            </div>
        </div>
    </footer>

</div>
<!-- Latest compiled and minified JavaScript -->
<script src="js/jquery.min.js"></script>
<script src="js/jquery1.min.js"></script>
<script src="plugin/dist/owl.carousel.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/number-count/jquery.counterup.min.js"></script>
<script src="js/isotope.pkgd.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/bootstrap-datepicker.min.js"></script>
<script src="js/bootstrap-datepicker.tr.min.js"></script>
<script src="js/moment.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script src="js/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>
<script src="js/picturefill.min.js"></script>
<script src="js/lightgallery.js"></script>
<script src="js/lg-pager.js"></script>
<script src="js/lg-autoplay.js"></script>
<script src="js/lg-fullscreen.js"></script>
<script src="js/lg-zoom.js"></script>
<script src="js/lg-hash.js"></script>
<script src="js/lg-share.js"></script>
<script src="js/jquery.nice-select.js"></script>
<script src="js/semantic.js"></script>
<script src="js/parallax.min.js"></script>
<script src="js/jquery.nicescroll.min.js"></script>
<script src="js/jquery.sticky.js"></script>
<script src="js/main.js"></script>
</body>

</html>