<?php include('header.php')?>
        <!--BENGIN CONTENT HEADER-->
        <section class="site-content-area mrg_top">
            <div class="container-fluid">
                <div class="row">
                    <div class="vk-contact-form">
                        <div class="container">
                            <div class="vk-contact-form-info-header">
                                <h2>Personal Information</h2>
								
                                <div class="clearfix"></div>
                                <div class="vk-contact-border"></div>
                            </div>
                            <div class="vk-contact-form-info-body">
                                
                                <div class="login_inside more_space">
                                    <form>
                                        <div class="form-group">
                                            <label> Name </label>
                                            <h6> <?php echo $view->name?></h6>
                                        </div>
                                        <div class="form-group">
                                            <label> Address </label>
                                            <h6> <?php echo $view->address?> </h6>
                                        </div>
                                        <div class="form-group">
                                            <label> Phone Number </label>
                                            <h6> <?php echo $view->contact?></h6>
                                        </div>
                                        <div class="form-group">
                                            <label> Unique Code </label>
                                            <h6> <?php echo $view->unique_id?> </h6>
                                        </div>
                                        <div class="form-group">
                                            <label> Credit Number </label>
                                            <h6> <?php echo $view->credit_number?> </h6>
                                        </div>
                                        <div class="form-group">
                                            <label> Email </label>
                                            <h6><?php echo $view->email?>  </h6>
                                        </div>
                                        <div class="form-group">
                                            <a href="<?php echo base_url('Home/editprofile/')?>" class="vk-btn  vk-btn-xs vk-btn-default text-uppercase">
                                                <span class="title">Edit Detail</span>
                                            </a>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--END CONTENT ABOUT-->

<?php include('footer.php')?>