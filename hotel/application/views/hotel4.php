  <?php include('header.php');?>

        <!--BENGIN CONTENT HEADER-->
        <section class="site-content-area mrg_top">
            <div class="container-fluid">
                <div class="row">

                    <div class="vk-restaurant-content">
                        <div class="vk-restaurant-time-for-meals">
                            <div class="container">
                                <div class="vk-restaurant-header">
                                    <h2> Hotel 4 </h2>
                                    <div class="vk-restaurant-border"></div>
                                </div>
                                <div class="vk-restaurant-time-text rest_list">
                                    <ul>
                                        <li> 4* Hotel </li>
                                        <li> Glyfada Beach, Logos, Paxoi, Greece </li>
                                        <li> +30 2662123456 </li>
                                    </ul>
                                </div>
                                <div class="vk-restaurant-authour">
                                    <p>Manager - <a href="#"> Mr. Grigoris Natsios </a></p>
                                </div>
                                <div class="vk-restaurant-delicious">

                                    <div class="vk-restaurant-header">
                                        <h2> Outer Space </h2>
                                        <div class="vk-restaurant-border"></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="vk-restaurant-item">
                                                <div class="vk-restaurant-delicious-img">
                                                    <a href="#"><img src="<?php echo base_url('frontend/')?>img/hotel4.png" alt="" class="img-responsive"></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="vk-restaurant-item">
                                                <div class="vk-restaurant-delicious-img">
                                                    <a href="#"><img src="<?php echo base_url('frontend/')?>img/hotel5.png" alt="" class="img-responsive"></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="vk-restaurant-item">
                                                <div class="vk-restaurant-delicious-img">
                                                    <a href="#"><img src="<?php echo base_url('frontend/')?>img/hotel6.png" alt="" class="img-responsive"></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="vk-restaurant-header" style="margin-top: 70px;">
                                        <h2> Inner Space </h2>
                                        <div class="vk-restaurant-border"></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="vk-restaurant-item">
                                                <div class="vk-restaurant-delicious-img">
                                                    <a href="#"><img src="<?php echo base_url('frontend/')?>img/inner1.png" alt="" class="img-responsive"></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="vk-restaurant-item">
                                                <div class="vk-restaurant-delicious-img">
                                                    <a href="#"><img src="<?php echo base_url('frontend/')?>img/inner2.png" alt="" class="img-responsive"></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="vk-restaurant-item">
                                                <div class="vk-restaurant-delicious-img">
                                                    <a href="#"><img src="<?php echo base_url('frontend/')?>img/inner3.png" alt="" class="img-responsive"></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="vk-room-list-content">
                                        <div class="row">
                                            <div class="col-md-12 vk-dark-our-room-item-right vk-clear-padding">
                                                <div class="vk-dark-our-room-item-content">
                                                    <h3><a href="#">Room Prices</a></h3>
                                                    <ul>
                                                        <li>
                                                            <p>
                                                                <img src="<?php echo base_url('frontend/')?>img/bed1.png">
                                                                Single Room <span> : 25$ / night</span>
                                                            </p>
                                                        </li>
                                                        <li>
                                                            <p>
                                                                <img src="<?php echo base_url('frontend/')?>img/bed1.png">
                                                                Double Room <span> : 40$ / night</span>
                                                            </p>
                                                        </li>
                                                        <li>
                                                            <p>
                                                                <img src="<?php echo base_url('frontend/')?>img/bed1.png">
                                                                Triple Room <span> :  55$ / night </span>
                                                            </p>
                                                        </li>
                                                        <li>
                                                            <p>
                                                                <img src="<?php echo base_url('frontend/')?>img/bed1.png"> 
                                                                Suite <span> : 180$ / night </span>
                                                            </p>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
        <!--END CONTENT ABOUT-->



       <?php include('footer.php');?>