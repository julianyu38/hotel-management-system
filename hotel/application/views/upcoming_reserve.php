
<?php include('header.php');?>
        <!--BENGIN CONTENT HEADER-->
        <section class="site-content-area mrg_top">
            <div class="container-fluid">
                <div class="row">

                    <div class="vk-room-list-content">
                        <div class="container">
                            <div class="vk-room-list-header">
                                <h2> Upcoming Reservations </h2>
                                <div class="vk-room-list-border"></div>
                            </div>
                            <?php foreach($view as $key)
                            {
                            $from=$key->from_date;
                            $newfrom = date("d-M-Y", strtotime($from));
                            $to=$key->to_date;
                            $newto = date("d-M-Y", strtotime($to));
                            ?>
                            <div class="row">
                                <div class="item">
                                    <div class="col-md-6 vk-dark-our-room-item-left  vk-clear-padding">
                                        <div class="vk-dark-our-room-item-img">
                                            <img src="<?php echo base_url('frontend/')?>img/img2.jpg" alt="" class="img-responsive">
                                        </div>
                                    </div>
                                    <div class="col-md-6 vk-dark-our-room-item-right vk-clear-padding">
                                        <div class="vk-dark-our-room-item-content">
                                            <!--<h3><a href="#"> First Class with AC </a></h3>-->
                                            <ul>
                                                <li><p><i class="fa fa-bed" aria-hidden="true"></i> Type of Room <span> : <?php echo $key->room_type; ?> BHK</span></p></li>
                                                <li><p><i class="fa fa-map-marker" aria-hidden="true"></i> Locations <span> :  <?php  echo $this->db->get_where('hotels',['id'=>$key->hotel_id])->row()->address; ?> </span></p></li>
                                                <li><p><i class="fa fa-star" aria-hidden="true"></i> Star <span> : <?php  echo $this->db->get_where('hotels',['id'=>$key->hotel_id])->row()->category; ?></span></p></li>
                                                <li><p><i class="fa fa-calendar" aria-hidden="true"></i> Date of Arrival <span> : <?php echo $newfrom; ?></span></p></li>
                                                <li><p><i class="fa fa-calendar" aria-hidden="true"></i> Date of Departure <span> :  <?php echo $newto; ?></span></p></li>
                                            </ul>
                                            <div class="vk-dark-our-room-item-book">
                                                <div class="vk-dark-our-room-item-book-left">
                                                    <ul>
                                                        <li>
                                                            <p>Price Per Night : </p>
                                                        </li>
                                                        <li>
                                                            <p>$<?php echo $key->price_per_night; ?>/ <span>Night</span></p>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="vk-dark-our-room-item-book-right">
                                                    <a href="<?php echo base_url('Home/cancel_booking')?>?id=<?php echo $key->id; ?>"  onClick="return confirm('are you sure want to Cancel..?')"> Cancel Reservation <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                           <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--END CONTENT ABOUT-->

       <?php include('footer.php');?>