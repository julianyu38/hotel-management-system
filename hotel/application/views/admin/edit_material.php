<?php include("header.php") ; ?>

<div class="main-content">
   <div class="content-wrapper"><!--Statistics cards Starts-->
        
      <div class="row">
    <div class="col-12">
        <div class="content-header">Material
		<a href="<?php echo base_url('admin/Materials');?>"> <button type="button" class="btn btn-raised btn-success btn-min-width mr-1 mb-1"  style="float: right;" >Back To List</button>
        </a>
        </div>
       
        
        <?php if($this->session->flashdata("message")){ ?>
        <div class="alert alert-icon-left alert-danger alert-dismissible mb-2" role="alert">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
		   <?php echo  $this->session->flashdata("message") ; ?>
        </div>
       <?php } ?> 
       <?php if($this->session->flashdata("successmessage")){ ?>
        <div class="alert alert-icon-left alert-success alert-dismissible mb-2" role="alert" style="margin-top: 15px;">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
		   <?php echo  $this->session->flashdata("successmessage") ; ?>
        </div>
       <?php } ?> 
        
    </div>
</div>



<section id="extended">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                         <form class="form" action="" method="post">
							<div class="form-body">
                                <h4 class="form-section"><i class="fa fa-bullhorn" aria-hidden="true"></i> Edit Material</h4>
                                <div class="form-group">
									<label for="eventRegInput4">Name</label>
									<input type="text" id="eventRegInput4" class="form-control" name="name" value="<?php echo $records->name; ?>" required>
								</div>
								 
                                
                            </div>

							<div class="form-actions center">
								<button type="reset" class="btn btn-raised btn-warning mr-1">
									<i class="ft-x"></i> Cancel
								</button>
								<button type="submit" name="userSubmit" value="save" class="btn btn-raised btn-primary">
									<i class="fa fa-check-square-o"></i> Save
								</button>
							</div>
						</form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>


  </div>
</div>
        
<?php include("footer.php") ; ?> 


            <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets') ;  ?>/vendors/css/tables/datatable/datatables.min.css">
        <script src="<?php echo base_url('assets') ;  ?>/js/data-tables/datatable-basic.js" type="text/javascript"></script>
         <script src="<?php echo base_url('assets') ;  ?>/vendors/js/datatable/datatables.min.js" type="text/javascript"></script>



