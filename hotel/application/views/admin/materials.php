<?php include("header.php") ; ?>

<div class="main-content">
   <div class="content-wrapper"><!--Statistics cards Starts-->
        
      <div class="row">
    <div class="col-12">
        <div class="content-header">All Materials
      <a href="<?php echo base_url('admin/Materials/add');?>"> <button type="button" class="btn btn-raised btn-success btn-min-width mr-1 mb-1"  style="float: right;" data-toggle="modal" data-target="#default">+ Add New</button>
        </a>
		</div>
        
        <?php if($this->session->flashdata("message")){ ?>
        <div class="alert alert-icon-left alert-danger alert-dismissible mb-2" role="alert">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
		   <?php echo  $this->session->flashdata("message") ; ?>
        </div>
       <?php } ?> 
       <?php if($this->session->flashdata("successmessage")){ ?>
        <div class="alert alert-icon-left alert-success alert-dismissible mb-2" role="alert" style="margin-top: 15px;">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
		   <?php echo  $this->session->flashdata("successmessage") ; ?>
        </div>
       <?php } ?> 
    </div>
</div>
<section id="extended">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"></h4>
                </div>
                <div class="card-body">
                    <div class="card-block">
                       <table class="table table-striped table-bordered multi-ordering" id="mytable">
                            <thead>
                                <tr>
                                    <th>#</th>
									<th>Name</th>
									<th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
							<?php if(!empty($view)){$i = 1 ; foreach($view as $vw){ ?>
                                <tr>
                                    <td><?php echo $i ; ?></td>
                                    <td><?php echo $vw->name ; ?></td>
									
									<td>
									  
                                        <a class="success p-0" title="Edit" href="<?php echo base_url('admin/Materials/edit/'.$vw->id);?>">
                                            <i class="ft-edit-2 font-medium-3 mr-2"></i>
                                        </a>
                                        <a class="danger p-0" title="Delete" href="<?php echo base_url('admin/Materials/delete/'.$vw->id) ?>"  onClick="return confirm('Are you sure want to delete..?')">
                                            <i class="ft-trash-2 font-medium-3 mr-2"></i>
                                        </a>
                                    </td>
                                </tr>

                               <?php $i++ ;  } }?> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


  </div>
</div>
 
 
        
<?php include("footer.php") ; ?> 