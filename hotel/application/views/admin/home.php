<?php include('header.php'); ?>
  <style>
  .bgchart{
     width:100%;
     height:100px;
     background: url('<?php base_url()?>/assets/img/graph.png');
     background-size: cover;
     background-position: center; 
     
  }
  
  </style>         
        <div class="main-content">
          <div class="content-wrapper"><!--Statistics cards Starts-->
<div class="row">
	<div class="col-xl-4 col-lg-6 col-md-6 col-12">
		<div class="card gradient-blackberry">
			<div class="card-body">
				<div class="card-block pt-2 pb-0">
					<div class="media">
						<div class="media-body white text-left">
							<h3 class="font-large-1 mb-0"><?php echo "1" ; ?></h3>
							<span>Total Services</span>
						</div>
						<div class="media-right white text-right">
							<i class="fa fa-list font-large-1"></i>
						</div>
					</div>
				</div>
				<div id="Widget-line-chart" class="height-75 WidgetlineChart WidgetlineChartshadow mb-2">					
				</div>
			</div>
		</div>
	</div>
	
	
	<div class="col-xl-4 col-lg-6 col-md-6 col-12">
		<div class="card gradient-green-tea">
			<div class="card-body">
				<div class="card-block pt-2 pb-0">
					<div class="media">
						<div class="media-body white text-left">
							<h3 class="font-large-1 mb-0"><?php echo "2"; ?></h3>
							<span>Total Material</span>
						</div>
						<div class="media-right white text-right">
							<i class="fa fa-list font-large-1"></i>
						</div>
					</div>
				</div>
				<div id="Widget-line-chart1" class="height-75 WidgetlineChart WidgetlineChartshadow mb-2 ">				
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-xl-4 col-lg-6 col-md-6 col-12">
		<div class="card gradient-ibiza-sunset">
			<div class="card-body">
				<div class="card-block pt-2 pb-0">
					<div class="media">
						<div class="media-body white text-left">
							<h3 class="font-large-1 mb-0"><?php echo "3" ; ?></h3>
							<span>Total Users</span>
						</div>
						<div class="media-right white text-right">
							<i class="fa fa-user font-large-1"></i>
						</div>
					</div>
				</div>
				<div id="Widget-line-chart2" class="height-75 WidgetlineChart WidgetlineChartshadow mb-2 ">					
				</div>

			</div>
		</div>
	</div>
	
	<?php /*<div class="col-xl-4 col-lg-6 col-md-6 col-12">
		<div class="card gradient-pomegranate">
			<div class="card-body">
				<div class="card-block pt-2 pb-0">
					<div class="media">
						<div class="media-body white text-left">
							<h3 class="font-large-1 mb-0"><?php echo count($active_user) ; ?></h3>
							<span>Active users</span>
						</div>
						<div class="media-right white text-right">
							<i class="fa fa-users font-large-1"></i>
						</div>
					</div>
				</div>
				<div id="Widget-line-chart3" class="height-75 WidgetlineChart WidgetlineChartshadow mb-2 ">					
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-xl-4 col-lg-6 col-md-6 col-12">
		<div class="card gradient-pomegranate">
			<div class="card-body">
				<div class="card-block pt-2 pb-0">
					<div class="media">
						<div class="media-body white text-left">
							<h3 class="font-large-1 mb-0"><?php echo count($inactive_user) ; ?></h3>
							<span>Inactive users</span>
						</div>
						<div class="media-right white text-right">
							<i class="fa fa-users font-large-1"></i>
						</div>
					</div>
				</div>
				<div id="Widget-line-chart4" class="height-75 WidgetlineChart WidgetlineChartshadow mb-2 ">					
				</div>
			</div>
		</div>
	</div> */ ?>
	
	
</div>
<!--Statistics cards Ends-->


	</div>
</div>
<?php include('footer.php'); ?>          

       
    
  