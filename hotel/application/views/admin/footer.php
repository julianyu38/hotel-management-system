 <footer class="footer footer-static footer-light">
          <p class="clearfix text-muted text-sm-center px-2"><span>Copyright  &copy; 2018 <a href="#" id="pixinventLink" target="_blank" class="text-bold-800 primary darken-2">RFQ </a>, All rights reserved. </span></p>
        </footer>

      </div> 
 </div>
    <!-- BEGIN VENDOR JS-->
    <script src="<?php echo base_url(); ?>assets/vendors/js/core/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/js/core/popper.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/js/core/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/js/prism.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/js/jquery.matchHeight-min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/js/screenfull.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/js/pace/pace.min.js" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="<?php echo base_url(); ?>assets/vendors/js/chartist.min.js" type="text/javascript"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN APEX JS-->
    <script src="<?php echo base_url(); ?>assets/js/app-sidebar.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/notification-sidebar.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/customizer.js" type="text/javascript"></script>
    <!-- END APEX JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="<?php echo base_url(); ?>assets/js/dashboard1.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS-->
	 <script src="<?php echo base_url('assets/') ; ?>vendors/js/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/') ; ?>js/data-tables/datatable-basic.js" type="text/javascript"></script> 



 <script>
        $(document).ready(function() {
    $('#mytable').dataTable();
    
     $("[data-toggle=tooltip]").tooltip();
    
} );

    </script>
  </body>

<!-- Mirrored from pixinvent.com/apex-angular-4-bootstrap-admin-template/html-demo-4/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 02 Apr 2018 06:34:19 GMT -->
</html>