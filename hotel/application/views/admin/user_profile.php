<?php include('header.php'); ?>

      
        <div class="main-content">
          <div class="content-wrapper"><!--User Profile Starts-->
		 
<!--Basic User Details Starts-->
<section id="user-profile">
    <div class="row">
        <div class="col-12">
            <div class="card profile-with-cover">
                <div class="card-img-top img-fluid bg-cover height-300" style="background: url('<?php echo base_url();?>assets/img/photos/14.jpg') 50%;"></div>
                <div class="media profil-cover-details row">
                    <div class="col-5">
                        <div class="align-self-start halfway-fab pl-3 pt-2">
                            <div class="text-left">
                                <h3 class="card-title white"><?php echo $user->fname.' '.$user->lname?></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="align-self-center halfway-fab text-center">
                            <a class="profile-image">
							<?php if($user->image !=''){ ?>
                                <img src="<?php echo base_url($user->image);?>" class="rounded-circle img-border gradient-summer width-100" alt="Card image">
                            <?php }else{ ?>
							<img src="<?php echo base_url('assets/img/portrait/avatars/avatar-08.png');?>" class="rounded-circle img-border gradient-summer width-100" alt="Card image">
							<?php } ?>
							</a>
                        </div>
                    </div>
                   
                </div>
               
            </div>
        </div>
    </div>
</section>
<!--Basic User Details Ends-->

<!--About section starts-->
<section id="about">
    <div class="row">
        <div class="col-12">
            <div class="content-header">About</div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>Personal Information</h5>
                </div>
                <div class="card-body">
                    <div class="card-block">
                       
                        <div class="row">
                            <div class="col-12 col-md-6 col-lg-4">
                                <ul class="no-list-style">
                                    <li class="mb-2">
                                        <span class="text-bold-500 primary"><a><i class="ft-user font-small-3"></i> First Name:</a></span>
                                        <span class="display-block overflow-hidden"><?php echo $user->fname; ?></span>
                                    </li>
                                    <li class="mb-2">
                                        <span class="text-bold-500 primary"><a><i class="ft-map-pin font-small-3"></i> Postcode:</a></span>
                                        <span class="display-block overflow-hidden"><?php echo $user->postcode; ?></span>
                                    </li>
                                    
                                </ul>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4">
                                <ul class="no-list-style">
                                    <li class="mb-2">
                                        <span class="text-bold-500 primary"><a><i class="ft-user font-small-3"></i> Last Name:</a></span>
                                        <span class="display-block overflow-hidden"><?php echo $user->lname; ?></span>
                                    </li>
                                    <li class="mb-2">
                                        <span class="text-bold-500 primary"><a><i class="fa fa-check font-small-3"></i> Status:</a></span>
                                        <a class="display-block overflow-hidden"><?php if($user->status == 1){ echo "<span style='color:green;'>Verified</span>" ; }else{ echo "<span style='color:red;'>Unverified</span>"; } ?></a>
                                    </li>
                                   
                                </ul>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4">
                                <ul class="no-list-style">
                                    <li class="mb-2">
                                        <span class="text-bold-500 primary"><a><i class="ft-mail font-small-3"></i> Email:</a></span>
                                        <span class="display-block overflow-hidden"><?php echo $user->email; ?></span>
                                    </li>
                                   
                                </ul>
                            </div>
                        </div>
                        <hr>
                        
            </div>
        </div>
        </div>
        </div>
    </div>
</section>
<!--About section ends-->


<!--User's uploaded photos section starts-->
<!--User Profile Starts-->
          </div>
        </div>
<?php include('footer.php') ;?>
      