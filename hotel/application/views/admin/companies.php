<?php include("header.php") ; ?>

<div class="main-content">
   <div class="content-wrapper"><!--Statistics cards Starts-->
        
      <div class="row">
    <div class="col-12">
        <div class="content-header">All Companies
      <!-- <a href="<?php echo base_url('admin/Menu/add');?>"> <button type="button" class="btn btn-raised btn-success btn-min-width mr-1 mb-1"  style="float: right;" data-toggle="modal" data-target="#default">+ Add New</button>
        </a>-->
		</div>
        
        <?php if($this->session->flashdata("message")){ ?>
        <div class="alert alert-icon-left alert-danger alert-dismissible mb-2" role="alert">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
		   <?php echo  $this->session->flashdata("message") ; ?>
        </div>
       <?php } ?> 
       <?php if($this->session->flashdata("successmessage")){ ?>
        <div class="alert alert-icon-left alert-success alert-dismissible mb-2" role="alert" style="margin-top: 15px;">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
		   <?php echo  $this->session->flashdata("successmessage") ; ?>
        </div>
       <?php } ?> 
    </div>
</div>
<section id="extended">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"></h4>
                </div>
                <div class="card-body">
                    <div class="card-block">
					
                       <table class="table table-striped table-bordered multi-ordering" id="mytable">
                            <thead>
                                <tr>
                                    <th>#</th>
									<th>Name</th>
									<th>Service</th>
									<th>Phone</th>
									<th>address</th>
									<th>city</th>
									<th>state</th>
									<th>country</th>
							 </tr>
                            </thead>
                            <tbody>
							<?php if(!empty($view)){$i = 1 ; foreach($view as $vw){ ?>
                                <tr>
                                    <td><?php echo $i ; ?></td>
									 <td><?php echo $vw->name ; ?></td>
                                     <td><?php echo $this->db->get_where("services",array("id"=>$vw->service))->row()->name ; ?></td>
                                     <td><?php echo $vw->contact ; ?></td>
                                     <td><?php echo $vw->address ; ?></td>
                                     <td><?php echo $this->db->get_where("cities",array("id"=>$vw->city_id))->row()->name ; ?></td>
                                     <td><?php echo $this->db->get_where("states",array("id"=>$vw->state_id))->row()->name ; ?></td>
                                     <td><?php echo $this->db->get_where("countries",array("id"=>$vw->country_id))->row()->name ; ?></td>
								</tr>

                               <?php $i++ ;  } }?> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


  </div>
</div>
 
 
        
<?php include("footer.php") ; ?> 
<script>
function featured(a,b){
	
	var val = a;
	var id = b;
	$.ajax({url: '<?php echo base_url(); ?>admin/Listings/featured/'+val+'/'+id, success: function(result)
     {
       // alert(result);
		 
	 } 
	 }); 
}
</script>