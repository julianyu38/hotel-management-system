 <?php include('header.php');?>
        <!--BENGIN CONTENT HEADER-->
         <section class="site-content-area mrg_top">
         <div class="vk-our-room">
                        <div class="container">
                            <div class="vk-sparta-head-title">
                                <h2>Our Hotels</h2>
                                <div class="vk-sparta-about-border"></div>
                            </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="vk-our-room padding-none ">
                            <div class="vk-sparta-our-room ">
                                 <div class="item">
                                    <div class="vk-sparta-item-content">
                                        <div class="vk-item-img">
                                            <a href="#"><img src="<?php echo base_url('frontend/')?>img/img.jpg" alt="" class="img-responsive"></a>
                                        </div>
                                        <div class="vk-item-text hotel_text">
                                            <h2><a href="<?php echo base_url('Home/hotel1')?>">Hotel 1</a></h2>
                                            <p> 3* Hotel </p>
                                            <p> Platy Beach, Limnos, Greece </p>
                                            <a href="<?php echo base_url('Home/hotel1')?>" class="vk-btn vk-btn-xs vk-btn-default text-uppercase">
                                                <span class="title">View</span>
                                            </a>
                                        </div>
                                    </div>
                                 </div>
                            </div>
                        </div>
                       
                    </div>
                     <div class="col-md-4 col-sm-6">
                        <div class="vk-our-room padding-none">
                            <div class="vk-sparta-our-room">
                                 <div class="item">
                                            <div class="vk-sparta-item-content">
                                                <div class="vk-item-img">
                                                    <a href="#"><img src="<?php echo base_url('frontend/')?>img/img1.jpg" alt="" class="img-responsive"></a>
                                                </div>
                                                <div class="vk-item-text hotel_text">
                                                    <h2><a href="<?php echo base_url('Home/hotel2')?>">Hotel 2</a></h2>
                                                    <p> 2* Hotel </p>
                                                    <p> Bouboulinas 1, Athens, Greece </p>
                                                    <a href="<?php echo base_url('Home/hotel2')?>" class="vk-btn vk-btn-xs vk-btn-default text-uppercase">
                                                        <span class="title">View</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                            </div>
                        </div>
                       
                    </div>
                     <div class="col-md-4 col-sm-6">
                        <div class="vk-our-room padding-none">
                            <div class="vk-sparta-our-room">
                                <div class="item">
                                            <div class="vk-sparta-item-content">
                                                <div class="vk-item-img">
                                                    <a href="#"><img src="<?php echo base_url('frontend/')?>img/img2.jpg" alt="" class="img-responsive"></a>
                                                </div>
                                                <div class="vk-item-text hotel_text">
                                                    <h2><a href="<?php echo base_url('Home/hotel3')?>">Hotel 3</a></h2>
                                                    <p> 1* Hotel </p>
                                                    <p> Pyrgos Thermi / Mytilini Area, Pyrgi... </p>
                                                    <a href="<?php echo base_url('Home/hotel3')?>" class="vk-btn vk-btn-xs vk-btn-default text-uppercase">
                                                         <span class="title">View</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                            </div>
                        </div>
                       
                    </div>
                     <div class="col-md-4 col-sm-6">
                        <div class="vk-our-room padding-none">
                            <div class="vk-sparta-our-room">
                                 <div class="item">
                                            <div class="vk-sparta-item-content">
                                                <div class="vk-item-img">
                                                    <a href="#"><img src="<?php echo base_url('frontend/')?>img/img.jpg" alt="" class="img-responsive"></a>
                                                </div>
                                                <div class="vk-item-text hotel_text">
                                                    <h2><a href="<?php echo base_url('Home/hotel4')?>">Hotel 4</a></h2>
                                                    <p> 4* Hotel </p>
                                                    <p>Glyfada Beach, Logos, Paxoi, Greece</p>
                                                    <a href="<?php echo base_url('Home/hotel4')?>" class="vk-btn vk-btn-xs vk-btn-default text-uppercase">
                                                         <span class="title">View</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                            </div>
                        </div>
                       
                    </div>
                     <div class="col-md-4 col-sm-6">
                        <div class="vk-our-room padding-none">
                            <div class="vk-sparta-our-room">
                                 <div class="item">
                                            <div class="vk-sparta-item-content">
                                                <div class="vk-item-img">
                                                    <a href="#"><img src="<?php echo base_url('frontend/')?>img/img1.jpg" alt="" class="img-responsive"></a>
                                                </div>
                                                <div class="vk-item-text hotel_text">
                                                    <h2><a href="<?php echo base_url('Home/hotel5')?>">Hotel 5</a></h2>
                                                    <p> 5* Hotel </p>
                                                    <p> Tourlos Mykonos, Mykonos, Greece </p>
                                                    <a href="<?php echo base_url('Home/hotel5')?>" class="vk-btn vk-btn-xs vk-btn-default text-uppercase">
                                                         <span class="title">View</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                            </div>
                        </div>
                       
                    </div>
                    
                    
                    
                </div>
            </div>
        </div>
        </section>
        <!--END CONTENT ABOUT-->


<?php include('footer.php');?>