<?php include('header.php'); ?>
<style>
#debt{
	color:green;
}
</style>
        <!--BENGIN CONTENT HEADER-->
        <section class="site-content-area mrg_top">
            <div class="container-fluid">
                <div class="row">
                    <div class="vk-contact-form">
                        <div class="container">
                            <div class="vk-contact-form-info-header reporting">
                                <h2> Aggregate Report </h2>
								
                                <div class="clearfix"></div>
                                <div class="vk-contact-border"></div>
                            </div>
                            <div class="vk-contact-form-info-body">
                                
                                <div class="report_sec">
                                    <p> At the end of each year what each hotel owes to each other: </p>

                                    <div class="report_part">
                                        <h3> Debts of Hotel_1 </h3>
                                        <ul>
                                            <li> Hotel_1 debts to Hotel_2 : <span id="debt"><?php echo $hotel1->hotel2?></span></li>
                                            <li> Hotel_1 debts to Hotel_3 : <span id="debt"><?php echo $hotel1->hotel3?></span></li>
                                            <li> Hotel_1 debts to Hotel_4 : <span id="debt"><?php echo $hotel1->hotel4?></span></li>
                                            <li> Hotel_1 debts to Hotel_5 : <span id="debt"><?php echo $hotel1->hotel5?></span></li>
                                        </ul>
                                    </div>

                                    <div class="report_part">
                                        <h3> Debts of Hotel_2 </h3>
                                        <ul>
                                            <li> Hotel_2 debts to Hotel_1 : <span id="debt"><?php echo $hotel2->hotel1?></span></li>
                                            <li> Hotel_2 debts to Hotel_3 : <span id="debt"><?php echo $hotel2->hotel3?></span></li>
                                            <li> Hotel_2 debts to Hotel_4 : <span id="debt"><?php echo $hotel2->hotel4?></span></li>
                                            <li> Hotel_2 debts to Hotel_5 : <span id="debt"><?php echo $hotel2->hotel5?></span></li>
                                        </ul>
                                    </div>
									
									 <div class="report_part">
                                        <h3> Debts of Hotel_3 </h3>
                                        <ul>
                                            <li> Hotel_3 debts to Hotel_1 : <span id="debt"><?php echo $hotel3->hotel1?></span></li>
                                            <li> Hotel_3 debts to Hotel_2 : <span id="debt"><?php echo $hotel3->hotel2?></span></li>
                                            <li> Hotel_3 debts to Hotel_4 : <span id="debt"><?php echo $hotel3->hotel4?></span></li>
                                            <li> Hotel_3 debts to Hotel_5 : <span id="debt"><?php echo $hotel3->hotel5?></span></li>
                                        </ul>
                                    </div>
									
									 <div class="report_part">
                                        <h3> Debts of Hotel_4 </h3>
                                        <ul>
                                            <li> Hotel_4 debts to Hotel_1 : <span id="debt"><?php echo $hotel4->hotel1?></span></li>
                                            <li> Hotel_4 debts to Hotel_2 : <span id="debt"><?php echo $hotel4->hotel2?></span></li>
                                            <li> Hotel_4 debts to Hotel_3 : <span id="debt"><?php echo $hotel4->hotel3?></span></li>
                                            <li> Hotel_4 debts to Hotel_5 : <span id="debt"><?php echo $hotel4->hotel5?></span></li>
                                        </ul>
                                    </div>
									
									 <div class="report_part">
                                        <h3> Debts of Hotel_5 </h3>
                                        <ul>
                                            <li> Hotel_5 debts to Hotel_1 : <span id="debt"><?php echo $hotel5->hotel1?></span></li>
                                            <li> Hotel_5 debts to Hotel_2 : <span id="debt"><?php echo $hotel5->hotel2?></span></li>
                                            <li> Hotel_5 debts to Hotel_3 : <span id="debt"><?php echo $hotel5->hotel3?></span></li>
                                            <li> Hotel_5 debts to Hotel_4 : <span id="debt"><?php echo $hotel5->hotel4?></span></li>
                                        </ul>
                                    </div>

                                    <!--<div class="report_part">-->
                                    <!--    <h3> Fullness of each month (in percents) </h3>-->
                                        
                                    <!--    <div class="all_hotels">-->
                                    <!--        <h5> Hotel1 </h5>-->

                                    <!--        <table class="table table-hover">-->
                                    <!--            <thead>-->
                                    <!--              <tr>-->
                                    <!--                <th>Jan</th>-->
                                    <!--                <th>Feb</th>-->
                                    <!--                <th>Mar</th>-->
                                    <!--                <th>Apr</th>-->
                                    <!--                <th>May</th>-->
                                    <!--                <th>Jun</th>-->
                                    <!--                <th>Jul</th>-->
                                    <!--                <th>Aug</th>-->
                                    <!--                <th>Sep</th>-->
                                    <!--                <th>Oct</th>-->
                                    <!--                <th>Nov</th>-->
                                    <!--                <th>Dec</th>-->
                                    <!--              </tr>-->
                                    <!--            </thead>-->
                                    <!--            <tbody>-->
                                    <!--              <tr>-->
                                    <!--                <td>20%</td>-->
                                    <!--                <td>19%</td>-->
                                    <!--                <td>76%</td>-->
                                    <!--                <td>36%</td>-->
                                    <!--                <td>82%</td>-->
                                    <!--                <td>49%</td>-->
                                    <!--                <td>70%</td>-->
                                    <!--                <td>68%</td>-->
                                    <!--                <td>55%</td>-->
                                    <!--                <td>45%</td>-->
                                    <!--                <td>18%</td>-->
                                    <!--                <td>16%</td>-->
                                    <!--              </tr>-->
                                    <!--            </tbody>-->
                                    <!--        </table>-->
                                    <!--    </div>-->
                                    <!--    <div class="all_hotels">-->
                                    <!--        <h5> Hotel2 </h5>-->

                                    <!--        <table class="table table-hover">-->
                                    <!--            <thead>-->
                                    <!--              <tr>-->
                                    <!--                <th>Jan</th>-->
                                    <!--                <th>Feb</th>-->
                                    <!--                <th>Mar</th>-->
                                    <!--                <th>Apr</th>-->
                                    <!--                <th>May</th>-->
                                    <!--                <th>Jun</th>-->
                                    <!--                <th>Jul</th>-->
                                    <!--                <th>Aug</th>-->
                                    <!--                <th>Sep</th>-->
                                    <!--                <th>Oct</th>-->
                                    <!--                <th>Nov</th>-->
                                    <!--                <th>Dec</th>-->
                                    <!--              </tr>-->
                                    <!--            </thead>-->
                                    <!--            <tbody>-->
                                    <!--              <tr>-->
                                    <!--                <td>20%</td>-->
                                    <!--                <td>19%</td>-->
                                    <!--                <td>76%</td>-->
                                    <!--                <td>36%</td>-->
                                    <!--                <td>82%</td>-->
                                    <!--                <td>49%</td>-->
                                    <!--                <td>70%</td>-->
                                    <!--                <td>68%</td>-->
                                    <!--                <td>55%</td>-->
                                    <!--                <td>45%</td>-->
                                    <!--                <td>18%</td>-->
                                    <!--                <td>16%</td>-->
                                    <!--              </tr>-->
                                    <!--            </tbody>-->
                                    <!--        </table>-->
                                    <!--    </div>-->
                                    <!--    <div class="all_hotels">-->
                                    <!--        <h5> Hotel3 </h5>-->

                                    <!--        <table class="table table-hover">-->
                                    <!--            <thead>-->
                                    <!--              <tr>-->
                                    <!--                <th>Jan</th>-->
                                    <!--                <th>Feb</th>-->
                                    <!--                <th>Mar</th>-->
                                    <!--                <th>Apr</th>-->
                                    <!--                <th>May</th>-->
                                    <!--                <th>Jun</th>-->
                                    <!--                <th>Jul</th>-->
                                    <!--                <th>Aug</th>-->
                                    <!--                <th>Sep</th>-->
                                    <!--                <th>Oct</th>-->
                                    <!--                <th>Nov</th>-->
                                    <!--                <th>Dec</th>-->
                                    <!--              </tr>-->
                                    <!--            </thead>-->
                                    <!--            <tbody>-->
                                    <!--              <tr>-->
                                    <!--                <td>20%</td>-->
                                    <!--                <td>19%</td>-->
                                    <!--                <td>76%</td>-->
                                    <!--                <td>36%</td>-->
                                    <!--                <td>82%</td>-->
                                    <!--                <td>49%</td>-->
                                    <!--                <td>70%</td>-->
                                    <!--                <td>68%</td>-->
                                    <!--                <td>55%</td>-->
                                    <!--                <td>45%</td>-->
                                    <!--                <td>18%</td>-->
                                    <!--                <td>16%</td>-->
                                    <!--              </tr>-->
                                    <!--            </tbody>-->
                                    <!--        </table>-->
                                    <!--    </div>-->
                                    <!--</div>-->
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--END CONTENT ABOUT-->

<?php include('footer.php'); ?>