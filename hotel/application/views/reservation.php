<?php include('header.php');?>
        <!--BENGIN CONTENT HEADER-->
		
        <section class="site-content-area mrg_top">
            <div class="container-fluid">
                <div class="row">

                    <div class="vk-room-list-content">
                        <div class="container">
                            <div class="vk-room-list-header">
                                <h2> Reservations </h2>
                                <div class="vk-room-list-border"></div>
                            </div>
                            <div class="reservation_sec">
                                <div class="reserv_inside">
                                    <div class="form-group">
                                        <label> Select Hotel </label>
                                        <select class="form-control" onchange="get_cat(this.value)" id="hotel" name="hotel_id">
										<option value=""> -Select Hotel- </option>
										<?php
										$qry="select *  from hotels ";
										$run=mysqli_query($conn,$qry);
										while($row = mysqli_fetch_assoc($run))
                                        {
										?>
										<option value="<?php echo $row['id']?>"> <?php echo $row['name']?> </option>
										<?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label> Select Room Type </label>
                                        <select class="form-control" onchange="get_price(this.value)" name="room_type">
                                            <option> -Select Type of Room- </option>
                                            <option value="1"> Single Room </option>
                                            <option value="2"> Double Room </option>
                                            <option value="3"> Triple Room </option>
                                            <option value="4"> Suit Room </option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label> Price / Night </label>
                                        <select class="form-control" id="price" name="price">
                                            <option> -Select Price / Night- </option>
                                            <option> 20$ / night </option>
                                            <option> 25$ / night </option>
                                            <option> 35$ / night </option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label> Select Location </label>
                                        <input type="text" class="form-control" placeholder="Location">
                                    </div>
                                    <div class="form-group">
                                        <label> Select Category </label>
                                        <select class="form-control" id="category" name="category">
                                            <option> -Select Category- </option>
                                            <option> 2* Hotel </option>
                                            <option> 3* Hotel </option>
                                            <option> 5* Hotel </option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label> Number of Room </label>
                                        <input type="text" class="form-control" placeholder="Number of Room" name="no_of_room">
                                    </div>
                                    <div class="form-group">
                                        <label> Availability </label>
                                        <div class="row">
                                            <div class="col-md-6" style="position: relative;">
                                                <div class="input-group date date-check-in">
                                                    <input name="date1" class="form-control" type="text" placeholder="Date of Arrival">
                                                    <span class="input-group-addon btn"><span class="ti-calendar" id="ti-calendar1"></span></span>
                                                </div>
                                            </div>
                                            <div class="col-md-6" style="position: relative;">
                                                <div class="input-group date date-check-out">
                                                    <input name="date2" class="form-control" type="text" placeholder="Date of Departure">
                                                    <span class="input-group-addon btn"><span class="ti-calendar" id="ti-calendar2"></span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" style="margin-top: 30px;">
                                        <a href="#" data-toggle="modal" data-target="#bookNow" class="vk-btn vk-btn-xs vk-btn-default text-uppercase">
                                            <span class="title">Book Now</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--END CONTENT ABOUT-->
<script>
function get_cat(id){
	var url ="<?php echo SITE_URL; ?>cat.php?hotel_id="+id;
			$.ajax({url: url, success: function(result){
				
            $("#category").html(result);
         }});
}
function get_price(type){
	var hotel_id = $( "#hotel" ).val();
	
	var url ="<?php echo SITE_URL; ?>ajax.php?hotel_id="+hotel_id+"&room_type="+type;
			$.ajax({url: url, success: function(result){
				
            $("#price").html(result);
         }});
}

</script>
       <?php include('footer.php');?>
<!--////////
            //////// Bootstrap Modal Example /////////
/////////-->

<div class="modal fade" id="bookNow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"> Book Now </h4>
        </div>
        <div class="modal-body">
            <div class="booking_sec">
                <div class="form-group">
                    <label> Name of Hotel </label>
                    <input type="text" class="form-control" placeholder="Name of Hotel">
                </div>
                <div class="form-group">
                    <label> Type of Room </label>
                    <select class="form-control">
                        <option> -Select Type of Room- </option>
                        <option> Single Room </option>
                        <option> Double Room </option>
                        <option> Triple Room </option>
                        <option> Suit Room </option>
                    </select>
                </div>
                <div class="form-group">
                    <label> Location </label>
                    <input type="text" class="form-control" placeholder="Location">
                </div>
                <div class="form-group">
                    <label> Category of Hotel </label>
                    <select class="form-control">
                        <option> -Select Category- </option>
                        <option> 3* hotel </option>
                        <option> 4* hotel </option>
                        <option> 5* hotel </option>
                    </select>
                </div>
                <div class="form-group">
                    <label> Sales of Reservation </label>
                    <input type="text" class="form-control" placeholder="Sales of Reservation">
                </div>
                <div class="form-group" style="margin-top: 30px;">
                    <a href="#" data-toggle="modal" data-target="#bookNow" class="vk-btn vk-btn-xs vk-btn-default text-uppercase">
                        <span class="title">Book Now</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>

</body>

</html>