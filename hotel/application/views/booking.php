<?php include('header.php');?>
        <!--BENGIN CONTENT HEADER-->
		
        <section class="site-content-area mrg_top">
            <div class="container-fluid">
                <div class="row">

                    <div class="vk-room-list-content">
                        <div class="container">
                            <div class="vk-room-list-header">
                                <h2> Reservations </h2>
                                <div class="vk-room-list-border"></div>
                            </div>
							<?php if($this->session->flashdata("successmessage")){ ?>
								<p style="text-align:center;color:green;"><?php echo $this->session->flashdata("successmessage")?></p>
							<?php } ?>
							<?php if($this->session->flashdata("errmessage")){ ?>
								<p style="text-align:center;color:red;"><?php echo $this->session->flashdata("errmessage")?></p>
							<?php } ?>
                            <div class="reservation_sec">
                                <div class="reserv_inside">
                                    <form action="<?php echo base_url('Booking/add')?>" method="post">
                                        
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label style="font-size: 18px; margin: 15px 0px;"> Number of selected type of room by hotel: </label>
                                            </div>
                                            <div class="col-md-3">
                                                <select class="form-control" name="room_type" id="room_type" onchange="get_detail(this.value)">
                                                    <option value=""> -Select Type of Room- </option>
                                                    <option value="1"> Single Room </option>
                                                    <option value="2"> Double Room </option>
                                                    <option value="3"> Triple Room </option>
                                                    <option value="4"> Suit Room </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                        
                                    <div class="form-group">
                                        <div class="row" style="margin: 0px;">
                                            <div class="col-md-2 less_padding">
                                                <label> Select hotel: </label>
                                            </div>
                                            <div class="col-md-2 less_padding">
                                                <label> Type of select room: </label>
                                            </div>
                                            <div class="col-md-2 less_padding">
                                                <label> Prise / night: </label>
                                            </div>
                                            <div class="col-md-2 less_padding">
                                                <label> Location: </label>
                                            </div>
                                            <div class="col-md-2 less_padding">
                                                <label> Select Category: </label>
                                            </div>
                                            <div class="col-md-2 less_padding">
                                                <label> No. of selected type of room: </label>
                                            </div>
                                        </div>
										<div id="detail"></div>
										
                                    </div>
									
									 <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label style="font-size: 18px; margin: 15px 0px;"> Select hotel: </label>
                                            </div>
                                            <div class="col-md-3">
                                                <select class="form-control" name="hotel_id" id="hotel_id">
                                                    <option value=""> -Select hotel- </option>
                                                    <option value="1"> Hotel 1</option>
                                                    <option value="2"> Hotel 2 </option>
                                                    <option value="3"> Hotel 3 </option>
                                                    <option value="4"> Hotel 4 </option>
													<option value="5"> Hotel 5 </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label> Avaibility: </label>
                                                <div class="row">
                                                    <div class="col-md-6" style="position: relative;">
                                                        <div class="input-group date date-check-in">
                                                            <input name="date1" class="form-control" id="from_date" type="text" placeholder="Date of Arrival">
                                                            <span class="input-group-addon btn"><span class="ti-calendar" id="ti-calendar1"></span></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6" style="position: relative;">
                                                        <div class="input-group date date-check-out">
                                                            <input name="date2" class="form-control" id="to_date" type="text" placeholder="Date of Departure">
                                                            <span class="input-group-addon btn"><span class="ti-calendar" id="ti-calendar2"></span></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <button style="max-width: 230px; display: block; margin: 0px; margin-top: 83px; text-transform: capitalize;" type="button" name="check" value="check" class="vk-btn vk-btn-xs vk-btn-default text-uppercase" onclick="chk_availability()">
                                                    <span class="title"> Check Avaibility </span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label style="font-size: 18px; margin: 0px;"> Number of available room for the selected hotel, type of room and given dates: </label>
                                            </div>
                                            <div class="col-md-3">
                                                <h6 id="roomno">  </h6>
												<input type="hidden" name="booking_room" value="1">
                                            </div>
                                        </div>
                                    </div>
									
                                    <div class="form-group" style="margin-top: 30px;">
                                        <section id="show_btn" style="display: block">
                                            <button style="max-width: 200px; display: block; margin: auto; margin-top: 60px;"  type="submit" name="booking" value="booking" class="vk-btn vk-btn-xs vk-btn-default text-uppercase" disabled>
                                                <span class="title">Book Now</span>
                                            </button>
                                        </section>
                                        <section id="show_content" style="display: none">
                                            <h5 style="color:red;"> "Sorry the selected Hotel cannot confirm the reservation for the given dates" </h5>
                                        </section>
										 <section id="show_another" style="display: none">
                                            <button style="max-width: 200px; display: block; margin: auto; margin-top: 60px;"  type="submit" name="available" value="available" class="vk-btn vk-btn-xs vk-btn-default text-uppercase" >
                                                <span class="title">Choose other</span>
                                            </button>
                                        </section>
                                    </div>
									</form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--END CONTENT ABOUT-->
		
<script>
function chk_availability(){
	var hotel_id = $( "#hotel_id" ).val();
	var room_type = $( "#room_type" ).val();
	var from_date = $( "#from_date" ).val();
	var to_date = $( "#to_date" ).val();
	
	var url ="<?php echo base_url(); ?>Booking/chk_availability";
	
			$.ajax({url: url,
			        type: 'post',
				    data: { "hotel_id":hotel_id,"room_type":room_type,"from_date":from_date,"booking_room":to_date },
            	   success: function(result){
			      
            if(result > 0){
				$('#show_content').hide();
				$('#show_another').hide();
				$('#show_btn').show();
				$('#roomno').html(result);
				$(':input[name="booking"]').prop('disabled', false);
			}else{
				$('#show_content').show();
				$('#show_another').show();
				$('#show_btn').hide();
				$('#roomno').html(0);
			}
			//$("#location").val(result);
         }});
}
function get_detail(type){
	
	var url ="<?php echo base_url(); ?>Booking/get_roomdet/"+type;
	   
			$.ajax({url: url, success: function(result){
			
            $("#detail").html(result);
         }});
}
function get_cat(id){
	
	var url ="<?php echo base_url(); ?>Home/get_cat/"+id;
	
			$.ajax({url: url, success: function(result){
			
            $("#category").html(result);
         }});
}
function get_price(type){
	var hotel_id = $( "#hotel" ).val();
	
	var url ="<?php echo base_url(); ?>Home/get_price/"+hotel_id+"/"+type;
	
			$.ajax({url: url, success: function(result){
				
            $("#price").html(result);
         }});
}
function get_location(){
	var hotel_id = $( "#hotel" ).val();
	
	var url ="<?php echo base_url(); ?>Home/get_location/"+hotel_id;
	
			$.ajax({url: url, success: function(result){
				
            $("#location").val(result);
         }});
}
function chk_room(booking_room){
	var hotel_id = $( "#hotel" ).val();
	var room_type = $( "#room_type" ).val();
	
	var url ="<?php echo base_url(); ?>Home/count_room";
	
			$.ajax({url: url,
			        type: 'post',
				    data: { "hotel_id":hotel_id ,"booking_room":booking_room ,"room_type":room_type },
            	   success: function(result){
			      
            if(result == 1){
				$('#err_msg').hide();
				$(':input[type="submit"]').prop('disabled', false);
			}else{
				$('#err_msg').show();
				$('#err_msg').html(result);
				$(':input[type="submit"]').prop('disabled', true);
			}
			//$("#location").val(result);
         }});
}

</script>
       <?php include('footer.php');?>
<!--////////
            //////// Bootstrap Modal Example /////////
/////////-->

<div class="modal fade" id="bookNow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"> Book Now </h4>
        </div>
        <div class="modal-body">
            <div class="booking_sec">
                <div class="form-group">
                    <label> Name of Hotel </label>
                    <input type="text" class="form-control" placeholder="Name of Hotel">
                </div>
                <div class="form-group">
                    <label> Type of Room </label>
                    <select class="form-control">
                        <option> -Select Type of Room- </option>
                        <option> Single Room </option>
                        <option> Double Room </option>
                        <option> Triple Room </option>
                        <option> Suit Room </option>
                    </select>
                </div>
                <div class="form-group">
                    <label> Location </label>
                    <input type="text" class="form-control" placeholder="Location">
                </div>
                <div class="form-group">
                    <label> Category of Hotel </label>
                    <select class="form-control">
                        <option> -Select Category- </option>
                        <option> 3* hotel </option>
                        <option> 4* hotel </option>
                        <option> 5* hotel </option>
                    </select>
                </div>
                <div class="form-group">
                    <label> Sales of Reservation </label>
                    <input type="text" class="form-control" placeholder="Sales of Reservation">
                </div>
                <div class="form-group" style="margin-top: 30px;">
                    <a href="#" data-toggle="modal" data-target="#bookNow" class="vk-btn vk-btn-xs vk-btn-default text-uppercase">
                        <span class="title">Book Now</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
