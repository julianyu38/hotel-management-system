<?php include('header.php');?>
        <!--BENGIN CONTENT HEADER-->
		
        <section class="site-content-area mrg_top">
            <div class="container-fluid">
                <div class="row">

                    <div class="vk-room-list-content">
                        <div class="container">
                            <div class="vk-room-list-header">
                                <h2> Reservations </h2>
                                <div class="vk-room-list-border"></div>
                            </div>
							<?php if($this->session->flashdata("successmessage")){ ?>
								<p style="text-align:center;color:green;"><?php echo $this->session->flashdata("successmessage")?></p>
							<?php } ?>
							<?php if($this->session->flashdata("errmessage")){ ?>
								<p style="text-align:center;color:red;"><?php echo $this->session->flashdata("errmessage")?></p>
							<?php } ?>
                            <div class="reservation_sec">
                                <div class="reserv_inside">
                                    <form action="" method="post">
									<div class="form-group">
                                        <label> Select Hotel </label>
                                       <input type="text" class="form-control" name="name" readonly value="<?php echo $hotel->name?>">
									   <input type="hidden" class="form-control" name="hotel_id" readonly value="<?php echo $hotel->id?>">
                                    </div>
                                    <div class="form-group">
                                        <label> Select Room Type </label>
                                        <input type="text" class="form-control" name="room_type" readonly value="<?php echo $hotelData->room_type?>">
                                    </div>
                                    <div class="form-group">
                                        <label> Price / Night </label>
                                        <input type="text" class="form-control" name="price" readonly value="<?php echo $hotelData->price?>">
                                    </div>
                                    <div class="form-group">
                                        <label> Select Location </label>
                                        <input type="text" class="form-control" id="location" name="location" placeholder="Location" required readonly value="<?php echo $hotel->address?>">
                                    </div>
                                    <div class="form-group">
                                        <label> Select Category </label>
                                       <input type="text" class="form-control" id="category" name="category" placeholder="Location" required readonly value="<?php echo $hotel->category?>">
                                    </div>
                                    <div class="form-group">
                                        <label> Number of Room </label>
                                        <input type="number" class="form-control" placeholder="Number of Room" name="no_of_room" readonly value="<?php echo $hotelData->number?>" required>
										<div id="err_msg"></div>
										<!--<span style="color:red;display:none;" id="err_msg">Not available</span>-->
                                    </div>
                                    <div class="form-group">
                                        <label> Availability </label>
                                        <div class="row">
                                            <div class="col-md-6" style="position: relative;">
                                                
                                                    <input name="from_date" class="form-control" type="text" placeholder="Date of Arrival" value="<?php echo $this->uri->segment('6')?>" readonly required>
                                                    
                                                
                                            </div>
                                            <div class="col-md-6" style="position: relative;">
                                               
                                                    <input name="to_date" class="form-control" type="text" placeholder="Date of Departure" value="<?php echo $this->uri->segment('7')?>" readonly required>
                                                    
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" style="margin-top: 30px;">
                                        <button type="submit" name="confirm" value="confirm" class="vk-btn vk-btn-xs vk-btn-default text-uppercase">
                                            <span class="title">Book Now</span>
                                        </button>
                                    </div>
									</form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--END CONTENT ABOUT-->
		
<script>
function get_cat(id){
	
	var url ="<?php echo base_url(); ?>Home/get_cat/"+id;
	
			$.ajax({url: url, success: function(result){
			
            $("#category").html(result);
         }});
}
function get_price(type){
	var hotel_id = $( "#hotel" ).val();
	
	var url ="<?php echo base_url(); ?>Home/get_price/"+hotel_id+"/"+type;
	
			$.ajax({url: url, success: function(result){
				
            $("#price").html(result);
         }});
}
function get_location(){
	var hotel_id = $( "#hotel" ).val();
	
	var url ="<?php echo base_url(); ?>Home/get_location/"+hotel_id;
	
			$.ajax({url: url, success: function(result){
				
            $("#location").val(result);
         }});
}
function chk_room(booking_room){
	var hotel_id = $( "#hotel" ).val();
	var room_type = $( "#room_type" ).val();
	
	var url ="<?php echo base_url(); ?>Home/count_room";
	
			$.ajax({url: url,
			        type: 'post',
				    data: { "hotel_id":hotel_id ,"booking_room":booking_room ,"room_type":room_type },
            	   success: function(result){
			      
            if(result == 1){
				$('#err_msg').hide();
				$(':input[type="submit"]').prop('disabled', false);
			}else{
				$('#err_msg').show();
				$('#err_msg').html(result);
				$(':input[type="submit"]').prop('disabled', true);
			}
			//$("#location").val(result);
         }});
}

</script>
       <?php include('footer.php');?>
<!--////////
            //////// Bootstrap Modal Example /////////
/////////-->

<div class="modal fade" id="bookNow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"> Book Now </h4>
        </div>
        <div class="modal-body">
            <div class="booking_sec">
                <div class="form-group">
                    <label> Name of Hotel </label>
                    <input type="text" class="form-control" placeholder="Name of Hotel">
                </div>
                <div class="form-group">
                    <label> Type of Room </label>
                    <select class="form-control">
                        <option> -Select Type of Room- </option>
                        <option> Single Room </option>
                        <option> Double Room </option>
                        <option> Triple Room </option>
                        <option> Suit Room </option>
                    </select>
                </div>
                <div class="form-group">
                    <label> Location </label>
                    <input type="text" class="form-control" placeholder="Location">
                </div>
                <div class="form-group">
                    <label> Category of Hotel </label>
                    <select class="form-control">
                        <option> -Select Category- </option>
                        <option> 3* hotel </option>
                        <option> 4* hotel </option>
                        <option> 5* hotel </option>
                    </select>
                </div>
                <div class="form-group">
                    <label> Sales of Reservation </label>
                    <input type="text" class="form-control" placeholder="Sales of Reservation">
                </div>
                <div class="form-group" style="margin-top: 30px;">
                    <a href="#" data-toggle="modal" data-target="#bookNow" class="vk-btn vk-btn-xs vk-btn-default text-uppercase">
                        <span class="title">Book Now</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>

</body>

</html>