<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Materials extends CI_Controller {
	       
		   function __construct()
			  {
				parent::__construct();
				$this->load->helper(array('form','url'));
				$this->load->library(array('form_validation', 'email'));
				$this->load->database();
				$this->load->model('admin/Home_model');
				 //Load session library
			   $this->load->library('session');
				
			  }

	public function index()
	  {
		$data['view']=$this->Home_model->selectdata("materials");
		
		$this->load->view('admin/materials',$data);
	  }
 
  
   
  
  public function delete($id) {	 
          $this->db->query("delete from materials where id='$id'");
		  $this->session->set_flashdata('successmessage', 'Materials has been deleted successfully*');
          redirect('admin/Materials');
      } 
      

public function add(){
	
        if($this->input->post('userSubmit')){
             /*  echo "<pre>";			  
			  print_r($_POST);
			  print_r($this->input->post('eng_option'));
			  print_r($this->input->post('eng_ingredients'));
			exit();  */ 
			
			$today = date('Y-m-d h:m:s') ;
            $userData = array(
			    'name' => $this->input->post('name'),
				'created_on' => $today 
            );
            
            //Pass user data to model
			$insert_id = $this->Home_model->insert('materials',$userData);
			
            //Storing insertion status message.
            if($insert_id){
               
                $this->session->set_flashdata('successmessage', 'Materials has been added successfully*');
				redirect('admin/Materials/add') ;
            }else{
                $this->session->set_flashdata('message', 'Some problems occured, please try again.');
				redirect('admin/Materials/add') ;
            }
        }
        //Form for adding user data
		
		$this->load->view('admin/add_material');
    }
	
public function edit($id) {
	
	  if($this->input->post('userSubmit')){
		  
		   $today = date('Y-m-d h:m:s') ;
			 
            $userData = array(
			    'name' => $this->input->post('name'),
               	'modified_on' => $today 
            );
            
            //Pass user data to model
			$insertUserData = $this->Home_model->update('materials',$id,$userData);
            if($insertUserData){
				$this->session->set_flashdata('successmessage', 'Materials has been Updated successfully*');
				redirect('admin/Materials/edit/'.$id) ;
				
			}
		  
	  }
	 $query = $this->db->get_where("materials",array("id"=>$id));
	 $data['records'] = $query->row(); 
	
	/*  echo "<pre>";
	 print_r($data);
	 exit(); */
	 $this->load->view('admin/edit_material',$data); 
	
}

 
	
	
	
	
}

