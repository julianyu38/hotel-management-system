<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Companies extends CI_Controller {
	       
		   function __construct()
			  {
				parent::__construct();
				$this->load->helper(array('form','url'));
				$this->load->library(array('form_validation', 'email'));
				$this->load->database();
				$this->load->model('admin/Home_model');
				 //Load session library
			   $this->load->library('session');
				
			  }

	public function index()
	  {
		$data['view']=$this->Home_model->get_tbl_data('company_listing',array());
		
		$this->load->view('admin/companies',$data);
	  }
	  
	    public function view($id) 
		{ 
         $data['user'] = $this->db->get_where("users",array("id"=>$id))->row();
	 $data['details'] = $this->db->get_where("user_detail",array("user_id"=>$id))->result();
         $this->load->view('admin/user_profile',$data); 
       }
 
}

