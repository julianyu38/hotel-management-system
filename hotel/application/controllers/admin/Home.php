<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	
	        function __construct()
			  {
				parent::__construct();
				$this->load->helper(array('form','url'));
				$this->load->library(array('form_validation', 'email'));
				$this->load->database();
				$this->load->model('admin/Login_admin_model','',TRUE);
				 $this->load->model('admin/Home_model');
				 //Load session library
			   $this->load->library('session');
				
			  }


public function index()
  {
	 
	    $this->load->view('admin/home');
 }

public function events(){
	$data['view']= $this->db->query("select * from events")->result() ;
	$this->load->view('admin/add_event',$data);
}
public function add_event(){
	$data['view']= '' ;
	$this->load->view('admin/add_event',$data);
}
  
  
  public function profile()
  {
	  $this->load->model('admin/Login_admin_model') ;
	  $adminc = $this->Login_admin_model->user_() ;
	  if($this->input->post('userSubmit'))
	  {
		  
		 if($this->input->post('old_password'))
		{ 
		     if($this->input->post('old_password') == $adminc->password)
			 {
			 $user_ar = array('username' => $this->input->post('username'),
							 'email' => $this->input->post('email'),
							 'web_email' => $this->input->post('web_email'),
							 //'currency' => $this->input->post('currency'),
							 'password' => $this->input->post('password')
							 );
							 
				  $this->Login_admin_model->user_up($user_ar) ;	
				  $this->session->set_flashdata("successmessage","Data Updated") ;		 
			 }
			 else
			 {
		       $this->session->set_flashdata("message","Old password is wrong") ;
			 }
		}
		else
		{
		 $user_ar = array('username' => $this->input->post('username'),
						 'email' => $this->input->post('email'),
						 'web_email' => $this->input->post('web_email'),
						// 'currency' => $this->input->post('currency'),
						  ) ;
			 $this->Login_admin_model->user_up($user_ar) ;
			 $this->session->set_flashdata("successmessage","Data Updated") ;					  
		}
	  }
	  $data["view"] = $this->Login_admin_model->user_() ;
      $this->load->view('admin/profile_setting',$data) ;
  }
  



  
      
}

