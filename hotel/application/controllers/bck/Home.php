<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	 function __construct()
  {
      parent::__construct();
    
      $this->load->model('Home_model','',TRUE);
      
    
  }
	public function index()
	{
		//$data["listings"] = $this->Home_model->get_tbl_data('listings',array('status'=>1));
		$data["featured"] = $this->db->query("select * from listings where status=1 and featured=1 order by id desc")->result();
		$data["latest"] = $this->db->query("select * from listings where status=1 order by id desc")->result();
		$data["mostview"] = $this->db->query("select * from listings where status=1 order by id desc")->result();
		$data["users"] = $this->db->query("select * from users where status=1 ")->result();
	    $data["state"] = $this->db->query("SELECT DISTINCT state FROM `state_detail`")->result();
		
		$this->load->view('index',$data);
	}
	 public function getsuburbhome()
  {
	  $id = $this->uri->segment('3');
	   //$id="ACT"; 
	  $data["suburb"] = $this->db->query("select distinct suburb from state_detail where state='$id'")->result();
	  $this->load->view('ajax/suburb_drop',$data);
  }
	public function getsuburb($id){
	    
		
		if($id != ''){
			//$id = 'ACT';
			//$states = $this->Home_model->getStateByCountry($id);
			$suburb =$this->db->query("select distinct suburb from state_detail where state='$id'")->result();
			
			$output = '<option value="">Select Suburb</option>';
			
			foreach($suburb as $sb){
				
				$output .= '<option value="'.$sb->suburb.'">'.$sb->suburb.'</option>';
				
				}
				
				echo $output;
			
			}
			die();
	}
		public function about()
	{
		$this->load->view('about');
	}
		public function contact()
	{
		$this->load->view('contact');
	}
		public function how_it_work()
	{
		$this->load->view('how-it-works');
	}
		public function faq()
	{
		$this->load->view('faq');
	}
	
	
	
	public function userSave(){
		
		    $email = $this->input->post('email');
			$password = sha1($this->input->post('password'));
			$fname = $this->input->post('fname');
			$lname = $this->input->post('lname');
			$postcode = $this->input->post('postcode');
			$businessname =$this->input->post('businessname');
	
			if($postcode !=""){
			   
				$postcode = $this->input->post('postcode');
				$businessname ='';
				$user_type = 1; 
			}else{
			    
				$businessname = $this->input->post('businessname');
				$postcode ='';
				$user_type = 2;
			}
		if($this->Home_model->wheredetail("users","email",$email)) {
				$response = array('class' => 'danger', 'field' => 'email', 'msg' => 'Email already exist.');
				echo json_encode($response);
				die;
			}
		    $time = date('Y-m-d H:i:s'); 
			$data =array(
						 'fname' =>$fname,
						 'lname' =>$lname,
						 'user_type' =>$user_type,
						 'email' =>$email,
						 'password' =>$password,
						 'postcode' =>$postcode,
						 'business_name' =>$businessname,
						 'status' =>0,
						 'created_on' =>$time
						); 
			$insert_id = $this->Home_model->insert('users',$data);
			
            if($user_type == 2){
			   $caterdata =array(
						 'cater_id' =>$insert_id,
						 'business_name' =>$businessname,
						 'created_on' =>$time
						); 	 
			  $insert_cater = $this->Home_model->insert('cater_detail',$caterdata);
				
			}else{
				$userData = array(
						 'user_id' =>$insert_id,
						 'postcode' =>$postcode,
						 'created_on' =>$time
						); 	 
			  $insert_user = $this->Home_model->insert('user_detail',$userData);
				
				
			}			
		    if($insert_id){
			$img_path = base_url('frontend/images/icater_logo.png');	
			$username = $fname.' '.$lname;
			$link = base_url('Home/verification/'.$insert_id);
			$mail ='<!DOCTYPE html>
<html lang="en">
<head>

  <title> My Private Pool </title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

<style type="text/css">
	
@media screen and (min-width:300px) and (max-width:617px){

.main_box{width: 100% !important;}
.logo{width: 35% !important; max-width: 100px;}
.card_box{width: 85% !important;}
.my_span{width: 63px !important;}
.align_left{float: left !important;}

.h2Width{font-size: 22px !important;}

}

</style>


</head>

<body style="margin: 0px auto; padding: 0px; background: #fff;">
  
<div class="main_box" style="width: 600px; margin: auto;">
	<div style="width: 100%; padding: 10px 0px; background: none; float: left;">
		<div class="logo" style="width: 18%; margin: 5px auto; float: none;"> <img style="width: 100%;" src="'.$img_path.'"> </div>
	</div>
	
	<div style="width: auto; background: #e8e8e8; padding: 40px 15px; float: left;">
		<div style="width: 100%; background: none; padding: 5px 0px; float: left;">
			<h2 class="h2Width" style="margin: 10px 0px;text-align: left;font-size: 22px;font-family: sans-serif;font-weight: 100; color: #b0cb1f">'.$username.', </h2>
			<p style="font-size: 16px; text-align: left;margin: 5px 0px; color: #333; font-weight: 100;font-family: sans-serif; line-height: 25px;"> You are only one step from being able to log in on our website!. Simply click on the link below to confirm your account: </p>
			<div style="width: 100%; background: none; padding: 10px 0px; text-align: center;">
			<a href="'.$link.'"><button style="padding: 10px 20px; background: #b0cb1f; color: #fff; border: none; margin: 10px 0px 0px; border-radius: 3px; font-size: 16px;"> Confirm Your Account </button></a>
			</div>
		</div>
	</div>

	<div style="width: 100%; padding: 5px 0px; background: #b0cb1f; float: left;">
		<p style="text-align: center;font-size: 14px;color: #fff;font-weight: 100;margin: 10px 0px;font-family: sans-serif;line-height: 20px;"> Copyright © 2018 iCater.com <br/> All Rights Reserved. </p>
	</div>
</div>

</body>

</html>';
                       
					  $config['protocol'] = 'sendmail';
                      $config['mailpath'] = '/usr/sbin/sendmail';
					  $config['charset'] = 'utf-8';
					  $config['wordwrap'] = TRUE;
					  $config['mailtype'] = 'html';
					  
					  $this->email->initialize($config);
					   
					  $this->load->library('email',$config);
                      $this->email->from('info@mobidudes.com', 'iCater');
					  $this->email->to($email); 
					  $this->email->subject('icater verification mail');   
					  $this->email->message($mail);   
					  $this->email->send();
    
				
				$msg = array('class'=>'success','msg'=>'Registere Successfully.');
			}else{
				$msg = array('class'=>'danger','msg'=>'Error has been occured.');
				
			}
		echo json_encode($msg);
		die;
	}
	/*	public function userreg(){
		
		    $email = $this->input->post('uemail');
			$password = sha1($this->input->post('upassword'));
			$fname = $this->input->post('ufname');
			$lname = $this->input->post('ulname');
			$postcode = $this->input->post('upostcode');
		
		if($this->Home_model->wheredetail("users","email",$email)) {
				$response = array('class' => 'danger', 'field' => 'email', 'msg' => 'Email already exist.');
				echo json_encode($response);
				die;
			}
		    $time = date('Y-m-d H:i:s'); 
			$data =array(
						 'fname' =>$fname,
						 'lname' =>$lname,
						 'user_type' =>1,
						 'email' =>$email,
						 'password' =>$password,
						 'postcode' =>$postcode,
						 'status' =>0,
						 'created_on' =>$time
						); 
			$insert_id = $this->Home_model->insert('users',$data);
			
           
				$userData = array(
						 'user_id' =>$insert_id,
						 'postcode' =>$postcode,
						 'created_on' =>$time
						); 	 
			  $insert_user = $this->Home_model->insert('user_detail',$userData);
					
		    if($insert_id){
			$img_path = base_url('frontend/images/icater_logo.png');	
			$username = $fname.' '.$lname;
			$link = base_url('Home/verification/'.$insert_id);
			$mail ='<!DOCTYPE html>
<html lang="en">
<head>

  <title> My Private Pool </title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

<style type="text/css">
	
@media screen and (min-width:300px) and (max-width:617px){

.main_box{width: 100% !important;}
.logo{width: 35% !important; max-width: 100px;}
.card_box{width: 85% !important;}
.my_span{width: 63px !important;}
.align_left{float: left !important;}

.h2Width{font-size: 22px !important;}

}

</style>


</head>

<body style="margin: 0px auto; padding: 0px; background: #fff;">
  
<div class="main_box" style="width: 600px; margin: auto;">
	<div style="width: 100%; padding: 10px 0px; background: none; float: left;">
		<div class="logo" style="width: 18%; margin: 5px auto; float: none;"> <img style="width: 100%;" src="'.$img_path.'"> </div>
	</div>
	
	<div style="width: auto; background: #e8e8e8; padding: 40px 15px; float: left;">
		<div style="width: 100%; background: none; padding: 5px 0px; float: left;">
			<h2 class="h2Width" style="margin: 10px 0px;text-align: left;font-size: 22px;font-family: sans-serif;font-weight: 100; color: #b0cb1f">'.$username.', </h2>
			<p style="font-size: 16px; text-align: left;margin: 5px 0px; color: #333; font-weight: 100;font-family: sans-serif; line-height: 25px;"> You are only one step from being able to log in on our website!. Simply click on the link below to confirm your account: </p>
			<div style="width: 100%; background: none; padding: 10px 0px; text-align: center;">
			<a href="'.$link.'"><button style="padding: 10px 20px; background: #b0cb1f; color: #fff; border: none; margin: 10px 0px 0px; border-radius: 3px; font-size: 16px;"> Confirm Your Account </button></a>
			</div>
		</div>
	</div>

	<div style="width: 100%; padding: 5px 0px; background: #b0cb1f; float: left;">
		<p style="text-align: center;font-size: 14px;color: #fff;font-weight: 100;margin: 10px 0px;font-family: sans-serif;line-height: 20px;"> Copyright © 2018 iCater.com <br/> All Rights Reserved. </p>
	</div>
</div>

</body>

</html>';
                       
					  $config['protocol'] = 'sendmail';
                      $config['mailpath'] = '/usr/sbin/sendmail';
					  $config['charset'] = 'utf-8';
					  $config['wordwrap'] = TRUE;
					  $config['mailtype'] = 'html';
					  
					  $this->email->initialize($config);
					   
					  $this->load->library('email',$config);
                      $this->email->from('info@mobidudes.com', 'iCater');
					  $this->email->to($email); 
					  $this->email->subject('icater verification mail');   
					  $this->email->message($mail);   
					  $this->email->send();
    
				echo "success";
				
			}else{
			
				
			}
	
	}*/

		
	public function verification($id)
		{
		if($id)
		{
			$this->Home_model->update("users",$id,array('status'=>1)) ;
			$this->session->set_flashdata('success', '<h2>Verified Successfully</h2><h4> Please login and continue your listing.!</h4>');  
			redirect(base_url('Home')) ;
		}
		else
		{
			redirect('Home') ;
		}
	}
	function contactus()
		{
		if($this->input->post('userSubmit'))
		{
			$time = date('Y-m-d H:i:s'); 
			$data =array(
						 'name' =>$this->input->post('name'),
						 'email' =>$this->input->post('con_email'),
						 'subject' =>$this->input->post('subject'),
						 'message' =>$this->input->post('message'),
						 'created_on' =>$time
						); 
			$insert_id = $this->Home_model->insert('contact_us',$data);
			if($insert_id){
				$this->session->set_flashdata('success', 'Successfull.We will contact you shortly');  
			    redirect('Home/contact') ;
			}else{
				$this->session->set_flashdata('err', 'Please try after some times');  
			    redirect('Home/contact') ;
			}
			
		}
		
	}
	public function forget(){
		if($this->input->post('userSubmit'))
		{
			$email = $this->input->post('forget_email');
			if($this->Home_model->wheredetail("users","email",$email)) {
				
				 $data["user_info"] = $this->Home_model->get_single_row('users',array('email'=>$email));
				
				 $this->load->view('change_password',$data);
				
			}else{
				$this->session->set_flashdata('err', 'Please Enter Valid Email-Id');  
			    redirect('Home/forget') ;
			}
		}
		$this->load->view('forget_pass');
	}
	
	public function change_password(){
		if($this->input->post('userSubmit'))
		{
			$newpassword = $this->input->post('new_password');
			$conpassword = $this->input->post('con_password');
			$user_id = $this->input->post('user_id');
			if($newpassword == $conpassword) {
				$today = date('Y-m-d h:m:s') ;
                $upData = array(
			    'password' => sha1($this->input->post('newpassword')),
				'modified_on' => $today 
             );
             $insert_id = $this->Home_model->update('users',$user_id,$upData);
             $this->session->set_flashdata('sucmessage', 'Password changed successfully');
				 redirect('Home/success') ;
				
			}else{
				$this->session->set_flashdata('err', 'Password mismatched!');  
			    redirect('Home/change_password/'.$user_id) ;
			}
		}
		$this->load->view('change_password');
	}
	public function success(){
		
		$this->load->view('success');
	}
	public function filter(){
		
		$state = $this->input->post('state');
		$suburb = $this->input->post('suburb');
		$data["listings"] = $this->db->query("select * from listings where state='$state' and suburb='$suburb'")->result();
		
		$this->load->view('search',$data);
		
	}

}
