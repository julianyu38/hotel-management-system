<?php
class User extends CI_Controller{

      function __construct()
  {
    parent::__construct();
    $this->load->helper(array('form','url','cookie')); 
    $this->load->library(array('form_validation', 'email'));
    $this->load->database();
    $this->load->model('Home_model','',TRUE);
     //Load session library
   $this->load->library('session');
    
  }


public function index()
  {
    $user_id = $this->session->userdata["users"]["user_id"];
	$data["userData"] = $this->db->get_where('users',['id'=>$user_id])->row();
	$data["services"] = $this->Home_model->get_tbl_data('services',array());
	$data["materials"] = $this->Home_model->get_tbl_data('materials',array());
	$data["countries"] = $this->Home_model->get_tbl_data('countries',array());
	
    $this->load->view('post',$data);
  }
public function myaccount()
	{
		$user_id = $this->session->userdata["users"]["user_id"];
		$data["postdata"]=$this->Home_model->get_tbl_data('post',array('user_id'=>$user_id));
	    $this->load->view('myaccount',$data);
	}
  public function addpost(){
	if($this->input->post('userSubmit')){
		
		if(!empty($_FILES['postimage']['name']))
					{
						
						$configs['upload_path'] = 'upload/postimage/';
						$configs['allowed_types'] = 'jpg|jpeg|png|gif';
						$configs['file_name'] = $_FILES['postimage']['name'];
						
						//Load upload library and initialize configuration
						$this->load->library('upload',$configs);
						$this->upload->initialize($configs);
						
							if($this->upload->do_upload('postimage'))
							{
								$uploadData = $this->upload->data();
								$postimage = "upload/postimage/".$uploadData['file_name'];
								
							}else{
								$postimage = '';
							}
					}else{
						$postimage = $this->input->post('old_img');
					}
					
					
		if(!empty($_FILES['document']['name']))
					{
						
						$configs['upload_path'] = 'upload/document/';
						$configs['allowed_types'] = '*';
						$configs['file_name'] = $_FILES['document']['name'];
						
						//Load upload library and initialize configuration
						$this->load->library('upload',$configs);
						$this->upload->initialize($configs);
						
							if($this->upload->do_upload('document'))
							{
								$uploadData = $this->upload->data();
								$document = "upload/document/".$uploadData['file_name'];
								
							}else{
								$document = '';
							}
					}else{
						$document = $this->input->post('old_img');
					}
					
					$today = date('Y-m-d H:m:s') ;
			 $cname = $this->db->get_where('cities',array("id"=>$this->input->post('city')))->row()->name ;
            $userData = array(
			        'user_id' => $this->input->post('user_id'),
				'rfq_no' => $this->input->post('rfq_no'),
                                'post_date' => $this->input->post('post_date'),
				'service' => $this->input->post('service'),
				'address' =>$this->input->post('address'),
				'country' =>$this->input->post('country'),
				'state' =>$this->input->post('state'),
				'city_id' =>$this->input->post('city'),
				'city' =>$cname,
				'material' =>$this->input->post('material'),
				'company' => $this->input->post('company'),
				'job_description' => $this->input->post('job_description'),
				'due_date' => $this->input->post('due_date'),
				'image' => $postimage,
				'document' => $document,
				'email' => $this->input->post('email'),
				'status' =>1,
				'created_on' => $today 
            );
            
            //Pass user data to model
			$insert_id = $this->Home_model->insert('post',$userData);
	        if($insert_id){
				$this->session->set_flashdata('successmessage', 'Post has been added successfully');
				redirect('User/') ;
				
			}
		
	}  
	$this->load->view('post',$data);
  }
  
  public function editpost(){
	  $user_id = $this->session->userdata["users"]["user_id"];
		$data["postdata"]=$this->Home_model->get_tbl_data('post',array('user_id'=>$user_id));
	    $this->load->view('myaccount',$data);
	}
  public function edit_post($id){
	  
	if($this->input->post('userSubmit')){
		
		if(!empty($_FILES['postimage']['name']))
					{
						
						$configs['upload_path'] = 'upload/postimage/';
						$configs['allowed_types'] = 'jpg|jpeg|png|gif';
						$configs['file_name'] = $_FILES['postimage']['name'];
						
						//Load upload library and initialize configuration
						$this->load->library('upload',$configs);
						$this->upload->initialize($configs);
						
							if($this->upload->do_upload('postimage'))
							{
								$uploadData = $this->upload->data();
								$postimage = "upload/postimage/".$uploadData['file_name'];
								
							}else{
								$postimage = '';
							}
					}else{
						$postimage = $this->input->post('old_postimage');
					}
					
					
		if(!empty($_FILES['document']['name']))
					{
						
						$configs['upload_path'] = 'upload/document/';
						$configs['allowed_types'] = '*';
						$configs['file_name'] = $_FILES['document']['name'];
						
						//Load upload library and initialize configuration
						$this->load->library('upload',$configs);
						$this->upload->initialize($configs);
						
							if($this->upload->do_upload('document'))
							{
								$uploadData = $this->upload->data();
								$document = "upload/document/".$uploadData['file_name'];
								
							}else{
								$document = '';
							}
					}else{
						$document = $this->input->post('old_document');
					}
					
					$today = date('Y-m-d H:m:s') ;
					 $cname = $this->db->get_where('cities',array("id"=>$this->input->post('city')))->row()->name ;
			 
              $userData = array(
			        'user_id' => $this->input->post('user_id'),
				//'rfq_no' => $this->input->post('rfq_no'),
                                'post_date' => $this->input->post('post_date'),
				'service' => $this->input->post('service'),
				'address' =>$this->input->post('address'),
				'country' =>$this->input->post('country'),
				'state' =>$this->input->post('state'),
				'city_id' =>$this->input->post('city'),
				'city' =>$cname,
				'material' =>$this->input->post('material'),
				'company' => $this->input->post('company'),
				'job_description' => $this->input->post('job_description'),
				'due_date' => $this->input->post('due_date'),
				'image' => $postimage,
				'document' => $document,
				'email' => $this->input->post('email'),
				'modified_on' => $today 
            );
            
            //Pass user data to model
			$update_id = $this->Home_model->update('post',$id,$userData);
	        if($update_id){
				$this->session->set_flashdata('successmessage', 'Post has been updated successfully');
				redirect('User/edit_post/'.$id) ;
				
			}
		
	}  
	
	$data["view"] = $this->Home_model->get_single_row('post',array('id'=>$id));
	$data["services"] = $this->Home_model->get_tbl_data('services',array());
	$data["materials"] = $this->Home_model->get_tbl_data('materials',array());
	$data["countries"] = $this->Home_model->get_tbl_data('countries',array());
	$this->load->view('edit_post',$data);
	  
	  
  }
  
  public function list_company(){
	  
	 $user_id = $this->session->userdata["users"]["user_id"];
	 $data["userData"] = $this->db->get_where('users',['id'=>$user_id])->row(); 
	 $data["countries"] = $this->Home_model->get_tbl_data('countries',array());
	 $data["services"] = $this->Home_model->get_tbl_data('services',array());
	 $data["comp_listing"] = $this->Home_model->get_single_row('company_listing',array('user_id'=>$user_id));
	 if(count($data["comp_listing"])>0){
		 $this->load->view('edit_listing',$data);
	 }else{
	 $this->load->view('company_listing',$data); 
	 }
  }
   public function get_state($country_id){
	   $states = $this->Home_model->get_tbl_data('states',array('country_id'=>$country_id));
	   echo "<option value=''>Select state</option>";
	     foreach($states as $st){
			 
			 echo "<option value=".$st->id.">".$st->name."</option>";
			 
			 
		 }
  }
   public function get_city($state_id){
	   $cities = $this->Home_model->get_tbl_data('cities',array('state_id'=>$state_id));
	   echo "<option value=''>Select city</option>";
	     foreach($cities as $cty){
			 
			 echo "<option value=".$cty->id.">".$cty->name."</option>";
			 
			 
		 }
  }
   public function add_listing(){
	   $user_id = $this->session->userdata["users"]["user_id"];
	   if($this->input->post('userSubmit')){
		
		if(!empty($_FILES['logo']['name']))
					{
						
						$configs['upload_path'] = 'upload/logo/';
						$configs['allowed_types'] = 'jpg|jpeg|png|gif';
						$configs['file_name'] = $_FILES['logo']['name'];
						
						//Load upload library and initialize configuration
						$this->load->library('upload',$configs);
						$this->upload->initialize($configs);
						
							if($this->upload->do_upload('logo'))
							{
								$uploadData = $this->upload->data();
								$logo = "upload/logo/".$uploadData['file_name'];
								
							}else{
								$logo = '';
							}
					}else{
						$logo = $this->input->post('old_img');
					}
				
					$today = date('Y-m-d H:m:s') ;
			 
            $userData = array(
			    'user_id' => $user_id,
			    'service' => $this->input->post('service'),
				'address' => $this->input->post('address'),
				'country_id' => $this->input->post('country'),
                'state_id' => $this->input->post('state'),
				'city_id' => $this->input->post('city'),
				'description' =>$this->input->post('description'),
				'contact' =>$this->input->post('contact'),
				'name' => $this->input->post('name'),
				'logo' => $logo,
				'created_on' => $today 
            );
            
            //Pass user data to model
			$insert_id = $this->Home_model->insert('company_listing',$userData);
	        if($insert_id){
				$this->session->set_flashdata('successmessage', 'Listing has been added successfully');
				redirect('User/list_company') ;
				
			}
  }
   }
   
    public function edit_listing(){
	   $user_id = $this->session->userdata["users"]["user_id"];
	   $list_id = $this->db->get_where('company_listing',['user_id'=>$user_id])->row()->id;
	   if($this->input->post('userSubmit')){
		
		if(!empty($_FILES['logo']['name']))
					{
						
						$configs['upload_path'] = 'upload/logo/';
						$configs['allowed_types'] = 'jpg|jpeg|png|gif';
						$configs['file_name'] = $_FILES['logo']['name'];
						
						//Load upload library and initialize configuration
						$this->load->library('upload',$configs);
						$this->upload->initialize($configs);
						
							if($this->upload->do_upload('logo'))
							{
								$uploadData = $this->upload->data();
								$logo = "upload/logo/".$uploadData['file_name'];
								
							}else{
								$logo = '';
							}
					}else{
						$logo = $this->input->post('old_img');
					}
				
					$today = date('Y-m-d H:m:s') ;
			 
            $userData = array(
			    'user_id' => $user_id,
				'service' => $this->input->post('service'),
				'address' => $this->input->post('address'),
				'country_id' => $this->input->post('country'),
                'state_id' => $this->input->post('state'),
				'city_id' => $this->input->post('city'),
				'description' =>$this->input->post('description'),
				'contact' =>$this->input->post('contact'),
				'name' => $this->input->post('name'),
				'logo' => $logo,
				'created_on' => $today 
            );
            
            //Pass user data to model
			$insert_id = $this->Home_model->update('company_listing',$list_id,$userData);
	        if($insert_id){
				$this->session->set_flashdata('successmessage', 'Listing has been updated successfully');
				redirect('User/list_company') ;
				
			}
  }
   }
   public function deletepost($id){
   
     $del = $this->Home_model->delete('post','id',$id);
     $this->session->set_flashdata('successmessage', 'Listing has been updated successfully');
     redirect('User/myaccount') ;
   }
   
   public function search(){
   
      if(isset($_REQUEST['so'])){
        $service_id = $_REQUEST['so'];
        $data["view"] = $this->Home_model->get_tbl_data('company_listing',array());
        }
	  $this->load->view('manufacture',$data);	
   
   }
public function logout() {
	$this->session->sess_destroy();
	redirect ('Home');
}

}

