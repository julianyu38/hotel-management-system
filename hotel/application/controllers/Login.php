<?php
class Login extends CI_Controller{

      function __construct()
  {
    parent::__construct();
    $this->load->helper(array('form','url','cookie')); 
    $this->load->library(array('form_validation', 'email'));
    $this->load->database();
    $this->load->model('Login_model','',TRUE);
     //Load session library
   $this->load->library('session');
    
  }


public function index()
  {
    //$data["name"] = $this->db->get('admin_user')->row()->username ; 
	//$data["logo"] = $this->db->get('admin_user')->row()->admin_logo ;  
    $this->load->view('login');
  }


        public function log() {
              
                         $email = $this->input->post('email');
					     $password = sha1($this->input->post('password')) ;
					     $user_id = $this->Login_model->login_user('users',$email,$password);
						 
						  if($user_id){
							          //create array of data
                                $user_data =array(
                                        'user_id'=>$user_id->id,
										'username'=>$user_id->name?:'',
										'email'=>$user_id->email,
										'logged_in'=> TRUE
                                       );
                             //set session userdata
                              
							   $logged_in = $this->session->set_userdata('users',$user_data);
							   redirect('User/');
							 
						  }else{
							    
								$this->session->set_flashdata('message', 'Invalid username or password');
				                redirect('Login') ;
                        }
						
						
						
				
				
        }
		
	
	  
                public function logout() {
                        $this->session->sess_destroy();
                        redirect ('Home');
                }
}

