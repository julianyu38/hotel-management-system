<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	 function __construct()
  {
      parent::__construct();
    
      $this->load->model('Home_model','',TRUE);
      
      
    
  }
 	public function test()
	{
	    $this->load->view('test');
	}
	
	public function index()
	{
	
		$data ="";
		$this->load->view('home',$data);
	}
	public function hotel()
	{
	    $this->load->view('hotel');
	}
	public function hotel1()
	{
	    $this->load->view('hotel1');
	}
	public function hotel2()
	{
	    $this->load->view('hotel2');
	}
	public function hotel3()
	{
	    $this->load->view('hotel3');
	}
	public function hotel4()
	{
	    $this->load->view('hotel4');
	}
	public function hotel5()
	{
	    $this->load->view('hotel5');
	}
	public function login()
	{
	    if($this->input->post('login')){
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$password = sha1($password) ;
			$userData = $this->Home_model->get_user($email,$password);
			if(!empty($userData)){
							//create array of data
					$admin_data =array(
							'user_id'=>$userData->id,
							'name'=>$userData->name,
							'username'=>$userData->username,
							'email'=>$userData->email,
							'contact'=>$userData->contact,
							'logged_in'=> TRUE
						   );
				 //ser session userdata
				   $logged_in = $this->session->set_userdata('user',$admin_data);
				   redirect('Home/');
			}else{
							//set error
					$this->session->set_flashdata('errmessage','Invalid Username or Password');
					redirect('Home/login/');
			}
		} 
		
		$this->load->view('login');
	}
	public function signup()
	{
	    if($this->input->post('save')){
			 if($this->Home_model->wheredetail(TBL_USER,"email",$this->input->post('email')))
						 {
							  $this->session->set_flashdata('errmessage', 'Email id already exists.');
							  redirect('Home/signup') ;
						 }
			if($this->Home_model->get_single_row(TBL_USER,array('username'=>$this->input->post('username'))))
						 {
							  $this->session->set_flashdata('errmessage', 'Username already exists.');
							  redirect('Home/signup') ;
						 }			 
			$usrArr = array(
			                'name'=>$this->input->post('name'),
							'unique_id'=>uniqid(),
							'username'=>$this->input->post('username'),
							'address'=>$this->input->post('address'),
							'email'=>$this->input->post('email'),
							'password'=>sha1($this->input->post('password')),
							'credit_number'=>$this->input->post('credit_number'),
							'contact'=>$this->input->post('contact'),
							'created_on'=>date('Y-m-d h:i:s')
							);
			$insert = $this->Home_model->insert(TBL_USER,$usrArr);
            if($insert){
				$this->session->set_flashdata('successmessage','You have registered successfully');
				redirect('Home/login');
			}else{
				$this->session->set_flashdata('message','Some problem occured in registration');
				redirect('Home/signup');
			}			
		}
		$data ="";
		$this->load->view('signup',$data);
	}
	
	public function get_cat($id=0){
			
	    
		if($id>0){
			
			$category = $this->Home_model->get_tbl_data(TBL_HOTEL,array('id'=>$id));
			
			$output = '<option value=""> -Select Category-</option>';
			
			foreach($category as $cat){
				
				$output .= '<option value="'.$cat->category.'">'.$cat->category.'</option>';
				
				}
				
				echo $output;
			
			}
			die();
	}
	public function get_price($hotel_id,$room_type){
			
	    
		if($hotel_id>0){
			
			$price = $this->Home_model->get_tbl_data(TBL_NOOFROOM,array('hotel_id'=>$hotel_id,'room_type'=>$room_type));
			
			$output = '<option value=""> -Select Price / Night-</option>';
			
			foreach($price as $prc){
				
				$output .= '<option value="'.$prc->price.'">'.$prc->price.'</option>';
				
				}
				
				echo $output;
			
			}
			die();
	}
	public function get_location($hotel_id){
		if($hotel_id>0){
			
			$hotel = $this->Home_model->get_single_row(TBL_HOTEL,array('id'=>$hotel_id));
			echo $hotel->address;
			
			}
			die();
	}
	public function count_room(){
		$hotel_id = $this->input->post('hotel_id');
		$room_type = $this->input->post('room_type');
		$booking_room = $this->input->post('booking_room');
		if($hotel_id>0){
			
			$hotel = $this->Home_model->get_single_row(TBL_NOOFROOM,array('hotel_id'=>$hotel_id,'room_type'=>$room_type));
			$available_rooms = $hotel->number;
			if($booking_room > $available_rooms){
				echo "<span style='color:red;'>There are maximum ".$available_rooms." rooms available <span>";
			}else{
				echo "1";
			}
			
			}
			die();
	}
	public function aggregate(){
		
		$data["page"] = "Aggregate";
		$data["debtData"] = $this->Home_model->get_tbl_data(TBL_DEBTH,array());
		$data["hotel1"] = $this->Home_model->get_single_row(TBL_DEBTH,array('id'=>1));
		$data["hotel2"] = $this->Home_model->get_single_row(TBL_DEBTH,array('id'=>2));
		$data["hotel3"] = $this->Home_model->get_single_row(TBL_DEBTH,array('id'=>3));
		$data["hotel4"] = $this->Home_model->get_single_row(TBL_DEBTH,array('id'=>4));
		$data["hotel5"] = $this->Home_model->get_single_row(TBL_DEBTH,array('id'=>5));
		$this->load->view('aggregate',$data);
		
	}
	public function logout(){
		$this->session->sess_destroy();
		redirect('Home');
	}
	
public function profile(){
	if(!isset($_SESSION['user']['user_id'])){
		redirect('Home');
	}else{
    $userid = $this->session->userdata['user']['user_id']; 
	$data["view"] = $this->Home_model->get_single_row(TBL_USER,array('id'=>$userid));
	$this->load->view('personal_info',$data);
	}
}
public function editprofile(){
	if(!isset($_SESSION['user']['user_id'])){
		redirect('Home');
	}
    $userid = $this->session->userdata['user']['user_id']; 
	if($this->input->post('userSubmit')){
		
		$userArr = array(
		                'name'=>$this->input->post('name'),
						'address'=>$this->input->post('address'),
						'contact'=>$this->input->post('contact'),
					    'credit_number'=>$this->input->post('credit_number'),
						'email'=>$this->input->post('email'),
						'updated_on'=>date('Y-m-d h:i:s'),
						);
			$update = $this->Home_model->update(TBL_USER,$userid,$userArr);
            if($update){
				$this->session->set_flashdata('successmessage','Your profile have updated successfully');
				redirect('Home/profile');
			}else{
				$this->session->set_flashdata('message','Some problem occured in profile update');
				redirect('Home/profile');
			}			
	}
	$data["view"] = $this->Home_model->get_single_row(TBL_USER,array('id'=>$userid));
	$this->load->view('profile_edit',$data);
}
	
	public function reservation(){
	if(!isset($_SESSION['user']['user_id'])){
		redirect('Home');
	}else{
    $userid = $this->session->userdata['user']['user_id']; 
	$data["view"] = $this->Home_model->wheredata('booking','user_id',$userid);
	$this->load->view('upcoming_reserve',$data);
	}
}

public function cancel_booking()
{
    if(!isset($_SESSION['user']['user_id'])){
		redirect('Home');
	}else{
    $userid = $this->session->userdata['user']['user_id'];
         $id=$_GET['id'];
         $this->Home_model->delete('booking','id',$id);
        redirect('Home/reservation');
	}
    
}
	
	
		

}
