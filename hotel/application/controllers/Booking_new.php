<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Booking extends CI_Controller {


	 function __construct()
  {
      parent::__construct();
    
      $this->load->model('Home_model','',TRUE);
      
      
    
  }
 	
	
public function index()
{

	$data ="";
	$data["hotels"]=$this->Home_model->get_tbl_data(TBL_HOTEL,array());
	$this->load->view('booking',$data);
}

public function add_booking(){
	
	if($this->input->post('booking')){
		
	    $from_date = date('Y-m-d',strtotime(str_replace('/','-',$this->input->post('date1'))));
		$to_date =	date('Y-m-d',strtotime(str_replace('/','-',$this->input->post('date2'))));
		$hotel_id = $this->input->post('hotel_id');
		$room_type = $this->input->post('room_type');
		$no_of_room = $this->input->post('no_of_room');
		
		$hotelData = $this->Home_model->get_single_row(TBL_NOOFROOM,array('hotel_id'=>$hotel_id));
		//echo "select SUM(no_of_booking_room) as qwe from booking where hotel_id=1 AND room_type=$room_type AND (DATE(from_date) BETWEEN DATE('$from_date') AND DATE('$to_date'))";
		//echo "select SUM(no_of_booking_room) as qwes from booking where hotel_id=1 AND room_type=$room_type AND (DATE(to_date) = DATE('$from_date'))";exit;
		/* echo $room_c1 = $this->db->query("select SUM(no_of_booking_room) as qwe from booking where hotel_id=1 AND room_type=$room_type AND (DATE(from_date)  BETWEEN DATE('$from_date') AND DATE('$to_date'))")->row()->qwe ;
		if($room_c1 !=''){
		echo 	$room_c2 = $this->db->query("select SUM(no_of_booking_room) as qwes from booking where hotel_id=1 AND room_type=$room_type AND (DATE(to_date) = DATE('$from_date'))")->row()->qwes ;
		  echo   $ag_room = $room_c1-$room_c2;
		}else{
			$ag_room = $room_c1;
		}
		exit;
		$available = $hotelData->number-$ag_room;
		if($available < $no_of_room){
			$this->session->set_flashdata('errmessage','Room is not available');
		    redirect('Booking');
			
		}else{ */
		//echo "select * from booking where hotel_id=$hotel_id AND room_type=1 AND '$from_date' >= from_date AND '$from_date' <= to_date";exit;
		$bookingData = $this->db->query("select * from booking where hotel_id=$hotel_id AND room_type=$room_type AND '$from_date' BETWEEN from_date and to_date")->row();
		if(!empty($bookingData)){
			$sq=1;
			$debth1 = $this->Home_model->get_single_row(TBL_DEBTH,array('id'=>$hotel_id));
			$amt1 = $this->db->query("select hotel".$hotel->id." as amt1 from debth")->row()->amt1;
			$hotels = $this->db->query("select * from hotels where id!=$hotel_id")->result();
			
			foreach($hotels as $ht){
				if($this->db->query("select * from booking where hotel_id=$ht->id AND room_type=$room_type AND '$from_date' BETWEEN from_date and to_date")->row()){
				
					echo "Not Available";
					//$this->session->set_flashdata('errmessage','No rooms available in this room type');
					//redirect('Booking');
				}else{
                    $amt2 = $this->db->query("select hotel".$ht->id." as amt from debth")->row()->amt2;
					echo $net_amt = $amt1-$amt2; exit;
					
					redirect('Booking/available/'.$hotel_id.'/'.$ht->id.'/'.$room_type.'/'.$from_date.'/'.$to_date);
					//echo $ht->id; echo "found";break;
				}
			}
			 
		}else{
		$bookArr = array(
			                'hotel_id'=>$this->input->post('hotel_id'),
							'user_id'=>$this->session->userdata["user"]["user_id"],
							'room_type'=>$this->input->post('room_type'),
							'price_per_night'=>$this->input->post('price'),
							'location'=>$this->input->post('location'),
							'category'=>$this->input->post('category'),
							'no_of_booking_room'=>$this->input->post('no_of_room'),
							'from_date'=>$from_date,
							'to_date'=>$to_date,
							'created_on'=>date('Y-m-d h:i:s')
							);
			$insert = $this->Home_model->insert(TBL_BOOKING,$bookArr);
            if($insert){
				$this->session->set_flashdata('successmessage','You have Booked succesfully');
				redirect('Booking');
			}else{
				$this->session->set_flashdata('message','Some problem occured in registration');
				redirect('Booking');
			}
		}
		}		
	
}
public function available($hotel_id,$available_hotel,$room_type){
	
	    $available_hotel = $available_hotel;
		
		if($this->input->post('confirm')){
			/* echo "<pre>";
			print_r($_POST);exit; */
			$bookArr = array(
			                'hotel_id'=>$this->input->post('hotel_id'),
							'user_id'=>$this->session->userdata["user"]["user_id"],
							'room_type'=>$this->input->post('room_type'),
							'price_per_night'=>$this->input->post('price'),
							'location'=>$this->input->post('location'),
							'category'=>$this->input->post('category'),
							'no_of_booking_room'=>$this->input->post('no_of_room'),
							'from_date'=>$this->input->post('from_date'),
							'to_date'=>$this->input->post('to_date'),
							'created_on'=>date('Y-m-d h:i:s')
							);
			$insert = $this->Home_model->insert(TBL_BOOKING,$bookArr);
            if($insert){
				$depth = $this->Home_model->get_single_row(TBL_DEBTH,array('id'=>$hotel_id));
				/* echo "<pre>";
				print_r($depth);exit; */
				$debth_arr = array('hotel'.$available_hotel=>0.1*($this->input->post('price')));
				$update = $this->Home_model->update(TBL_DEBTH,$hotel_id,$debth_arr);
				
				$this->session->set_flashdata('successmessage','You have Booked succesfully');
				redirect('Booking');
			}else{
				$this->session->set_flashdata('message','Some problem occured in registration');
				redirect('Booking');
			}
			
		}
	    $data["hotelData"] = $this->Home_model->get_single_row(TBL_NOOFROOM,array('hotel_id'=>$available_hotel,'room_type'=>$room_type));
		$data["hotel"] =  $this->Home_model->get_single_row(TBL_HOTEL,array('id'=>$available_hotel));
	
		$this->load->view('available',$data);
	
}
	
	
	
		

}
