<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {


	 function __construct()
  {
      parent::__construct();
    
      $this->load->model('Home_model','',TRUE);
  }
 	
	
public function index()
{
     if($this->input->post('login')){
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$adminData = $this->Home_model->get_admin($username,$password);
			
			if(!empty($adminData)){
							//create array of data
					$admin_data =array(
							'user_id'=>$adminData->id,
							'username'=>$adminData->username,
							'logged_in'=> TRUE
						   );
				 //ser session userdata
				   $logged_in = $this->session->set_userdata('admin',$admin_data);
				   redirect('Admin/aggregate');
			}else{
							//set error
					$this->session->set_flashdata('errmessage','Invalid Username or Password');
					redirect('Admin/');
			}
		} 
	$data ="";
	$this->load->view('admin_login',$data);
}
public function aggregate(){
	$data["page"] = "Aggregate";
	$data["debtData"] = $this->Home_model->get_tbl_data(TBL_DEBTH,array());
	$data["hotel1"] = $this->Home_model->get_single_row(TBL_DEBTH,array('id'=>1));
	$data["hotel2"] = $this->Home_model->get_single_row(TBL_DEBTH,array('id'=>2));
	$data["hotel3"] = $this->Home_model->get_single_row(TBL_DEBTH,array('id'=>3));
	$data["hotel4"] = $this->Home_model->get_single_row(TBL_DEBTH,array('id'=>4));
	$data["hotel5"] = $this->Home_model->get_single_row(TBL_DEBTH,array('id'=>5));
	$this->load->view('aggregate',$data);
}
public function logout(){
		$this->session->sess_destroy();
		redirect('Admin/');
	}
	

}
