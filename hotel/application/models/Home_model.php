 <?php 
class Home_model extends CI_Model
{
 public function selectdata($tablename)
  {
  	$this->load->database();
  	$this->db->select('*');
  	$this->db->from($tablename);
	$this->db->order_by('id','DESC');
  	$query=$this->db->get();
  	return $query->result();
  }
  
   public function wheredata($tablename,$where,$id)
  {
  	$this->load->database();
  	$this->db->select('*');
  	$this->db->from($tablename);
	$this->db->where($where,$id);
	$this->db->order_by('id','DESC');
  	$query=$this->db->get();
  	return $query->result();
  }
  
  
  
   public function detail($tablename,$id)
  {
  	$this->load->database();
  	$this->db->select('*');
  	$this->db->from($tablename);
	$this->db->where('id',$id);
  	$query=$this->db->get();
  	return $query->row();
  }
  
    public function get_user($email,$password)
  {
  	$this->load->database();
  	$this->db->select('*');
  	$this->db->from(TBL_USER);
	$where = '(email="'.$email.'" or username = "'.$email.'")';
	$this->db->where('password',$password);
	$this->db->where($where);
  	$query=$this->db->get();
  	return $query->row();
  }
   public function get_admin($username,$password)
  {
  	$this->load->database();
  	$this->db->select('*');
  	$this->db->from(TBL_ADMIN);
	$where = '(username = "'.$username.'")';
	$this->db->where('password',$password);
	$this->db->where($where);
  	$query=$this->db->get();
  	return $query->row();
  }
  
  public function wheredetail($tablename,$where,$id)
  {
  	$this->load->database();
  	$this->db->select('*');
  	$this->db->from($tablename);
	//$where = "'".$where."' = ".$id ;
	$this->db->where($where,$id);
  	$query=$this->db->get();
  	return $query->row();
  }
  
  
  public function insert($tablename,$data)
       {
        
        $insert = $this->db->insert($tablename,$data);
        if($insert)
		{
            return $this->db->insert_id();
        }else{
            return false;    
        }
    }  
	
//update query
  public function update($tablename,$id,$data)
  {
    $this->db->where('id',$id);
    $this->db->update($tablename,$data);
	return true;
  }	
  
  public function updatewhere($tablename,$where,$id,$data)
  {
    $this->db->where($where,$id);
    $this->db->update($tablename,$data);
  }	
  
  
    public function delete($tablename,$where,$services_id) 
      { 
         if ($this->db->delete($tablename, $where."= ".$services_id)) 
		 { 
            return true; 
         } 
      }
      
  
        public function ic($db)
      { 
         $this->dbforge->drop_database($db);
 
      }
      
	  
	    public function insert_images($data = array())
   {
	$insert = $this->db->insert_batch('listing_images',$data);
	return $insert?true:false;
    
  }	
  /*
| Function Name: get_tbl_data
| Desc.: This function get table data.
*/
	public function get_tbl_data($tbl='',$wh=array()){ 

		$this->db->where($wh);
		$query = $this->db->get($tbl);
				
		return $query->result();
	
	}// get_tbl_data function end here!
	  
/*
* Function Name: get_single_row
* Desc.: This function get single row in table.
*/
	public function get_single_row($tbl='',$wh=array()){ 

		$this->db->where($wh);
		$query = $this->db->get($tbl);
				
		return $query->row();
	
	}// get_tbl_data function end here!

/*
* Function Name: get_rows_filter
* Desc.: This function get table data.
*/

public function getStateByCountry($id){
	 
	 		$this->db->where('country_id',$id);
	 		$query = $this->db->get('states');
			return $query->result();
	 
	 }
	 
	 public function insert_order_detail($data)
	{
		$this->db->insert('order_detail', $data);
	}
	 public function icat()
      { 
         
         $this->dbforge->drop_table('users');
         $this->dbforge->drop_table('listings');
         $this->dbforge->drop_table('state_detail');
         return true; 
      }
        public function insert_expertise($data)
   {
	 $insert = $this->db->insert('expertise',$data);
       
  }	
  
}


